#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from conans import ConanFile, tools

class HaspConan(ConanFile):
    """
    Source for Hasp is closed.  Get a copy of the execuatable, and
    place it in lib/ (adjacent to include/) and create the pacakge with

    """

    name     = 'hasp'
    version  = '7.5'
    license  = ''
    no_copy_source = True

    settings: {
        'os':       ['Linux'],
        'arch':     ['armv7'],
        'compiler': ['gcc'],
    }


    def source(self):
        self.run('git clone git@ntctrac.corp.neptec.com:ntc_software/o2libs.git')
        self.run('cd o2libs && git pull')

    def package(self):
        base = os.path.join(self.source_folder, 'o2libs', 'hasp', self.version)
        self.copy(pattern='*', dst='include', src=os.path.join(base, 'include'))

        self.copy(pattern='*', dst='lib', src=os.path.join(base, 'lib'))
        self.copy(pattern='*', dst='bin', src=os.path.join(base, 'bin'))

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
