#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import os, shutil, re, glob
from six import StringIO  # Python 2 and 3 compatible<Paste>
from conans import ConanFile, tools, AutoToolsBuildEnvironment


class ncursesConan(ConanFile):
    name            = 'ncurses'
    version         = '6.1'
    md5_hash        = '98c889aaf8d23910d2b92d65be2e737a'
    license         = ''
    url             = 'https://ftp.gnu.org/pub/gnu/ncurses/'
    description     = 'ncurses is a programming library providing an API, allowing the programmer to write text user interfaces in a terminal-independent manner.'
    settings        = 'os', 'compiler', 'build_type', 'arch'
    requires        = 'helpers/[>=0.3]@ntc/stable'
    options = {
        'deploy_prefix': 'ANY',
        'without_progs': [True, False],
        'without_tack':  [True, False],
        'without_tests': [True, False],
    }
    default_options = (
        'deploy_prefix=',
        'without_progs=True',
        'without_tack=True',
        'without_tests=True',
    )

    def source(self):
        archive = f'ncurses-{self.version}'
        archive_ext = '.tar.gz'
        url = f'https://ftp.gnu.org/pub/gnu/ncurses/{archive}{archive_ext}'
        filename = os.path.basename(url)
        tools.download(url=url, filename=filename)

        # Ironically, I don't know how to use the .sig file, so we use the md5hash
        tools.check_md5(filename, self.md5_hash)

        tools.unzip(filename)
        shutil.move(archive, self.name)

    def build(self):

        args = []
        if 'TARGETMACH' in os.environ:
            args.append('--host=%s'%os.environ['TARGETMACH'])

        args.append(f'--prefix={self.package_folder}')
        args.append('--with-shared')
        args.append('--enable-widec')
        args.append('--without-debug')

        # Cannot enable this without the install target attempting to install
        # these pc files to system paths
        args.append('--enable-pc-files')
        args.append('--with-pkg-config-libdir=%s'%os.path.join(self.package_folder, 'lib', 'pkgconfig'))

        if self.options.without_progs:
            args.append('--without-progs')

        if self.options.without_tack:
            args.append('--without-tack')

        if self.options.without_tests:
            args.append('--without-tests')

        autotools = AutoToolsBuildEnvironment(self)
        with tools.chdir(self.name):
            autotools.configure(args=args)
            autotools.make()

    def package(self):
        autotools = AutoToolsBuildEnvironment(self)
        with tools.chdir(self.name):
            autotools.make(args=['install'])

        # Some configure options result in broken links, which will fail conan,
        # so we have to clean those up.
        mybuf = StringIO()
        self.run("find \"%s\" -xtype l;"%self.package_folder, output=mybuf)
        potentially_broken = mybuf.getvalue().split('\n')
        # self.output.info("potentially brokenn: %s"%'\n -'.join(potentially_broken))

        for f in potentially_broken:
            if not os.path.islink(f): continue

            if not self.valid_symlink(f):
                self.output.info('Removing broken symlink %s'%f)
                os.remove(f)

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)
        self.env_info.manpath.append(os.path.join(self.package_folder, 'share', 'man'))

        # Populate the pkg-config environment variables
        with tools.pythonpath(self):
            from platform_helpers import adjustPath, appendPkgConfigPath

            pkg_config_path = os.path.join(self.package_folder, 'lib', 'pkgconfig')
            appendPkgConfigPath(adjustPath(pkg_config_path), self.env_info)

            pc_files = glob.glob(adjustPath(os.path.join(pkg_config_path, '*.pc')))
            for f in pc_files:
                p_name = re.sub(r'\.pc$', '', os.path.basename(f))
                p_name = re.sub(r'\W', '_', p_name.upper())
                setattr(self.env_info, f'PKG_CONFIG_{p_name}_PREFIX', adjustPath(self.package_folder))

            appendPkgConfigPath(adjustPath(pkg_config_path), self.env_info)

    def package_id(self):
        """ Undo the effect that the deploy_prefix has on the package_id """
        self.info.options.deploy_prefix = ''

    def deploy(self):
        self.output.info('Start deploying')
        if len(str(self.options.deploy_prefix)):
            try:
                for d in self.cpp_info.libdirs:
                    self.copy(pattern='*', src=os.path.join(self.package_folder, d), dst=os.path.join(str(self.options.deploy_prefix), d), symlinks=True)
                for d in self.cpp_info.includedirs:
                    self.copy(pattern='*', src=os.path.join(self.package_folder, d), dst=os.path.join(str(self.options.deploy_prefix), d), symlinks=True)
                for d in self.cpp_info.bindirs:
                    self.copy(pattern='*', src=os.path.join(self.package_folder, d), dst=os.path.join(str(self.options.deploy_prefix), d), symlinks=True)

                d = 'share'
                self.copy(pattern='*', src=os.path.join(self.package_folder, d), dst=os.path.join(str(self.options.deploy_prefix), d), symlinks=True)
            except PermissionError:
                self.output.warn("Could not copy assets to %s."%str(self.options.deploy_prefix))

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
