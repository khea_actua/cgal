#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import os, shutil
from conans import ConanFile, CMake, tools
from conans.model.version import Version

class VtkConan(ConanFile):
    """
    Tested with 8.1.0, 7.1.1, 6.1.0

    Note, version 7 and above requires CMake 3.7+ (load it as part of the profile)
    """

    name        = 'vtk'
    description = 'Visualisation Toolkit'
    license     = 'BSD 3'
    url         = 'https://www.vtk.org/'
    settings    = 'os', 'compiler', 'build_type', 'arch'
    generators  = 'cmake'
    requires    = 'helpers/[>=0.3]@ntc/stable'
    options         = {
        'shared':  [True, False],
        'fPIC':    [True, False],
        'with_qt': [True, False],
    }
    default_options = ('shared=True', 'fPIC=True', 'with_qt=False')

    def requirements(self):
        if self.options.with_qt:
            self.requires('qt/[>=5.3.2]@ntc/stable')

    def config_options(self):
        if self.settings.compiler == "Visual Studio":
            self.options.remove("fPIC")

    def source(self):
        hashes = {
            '8.1.1': 'cf078a71c298c76b13707c7c27704248',
            '8.1.0': '4fa5eadbc8723ba0b8d203f05376d932',
            '7.1.1': 'daee43460f4e95547f0635240ffbc9cb',
            '6.1.0': '25e4dfb3bad778722dcaec80cd5dab7d',
        }

        archive = f'VTK-{self.version}.tar.gz'
        from source_cache import copyFromCache
        if copyFromCache(archive):
            tools.unzip(archive)
            shutil.move(f'VTK-{self.version}', 'VTK')
        else:
            if self.version in hashes:
                version_major = '.'.join(self.version.split('.')[:2])
                tools.download(
                    url=f'https://www.vtk.org/files/release/{version_major}/{archive}',
                    filename=archive
                )
                tools.check_md5(archive, hashes[self.version])
                tools.unzip(archive)
                shutil.move(f'VTK-{self.version}', 'VTK')
            else:
                self.run('git clone https://gitlab.kitware.com/vtk/vtk VTK')
                self.run(f'cd VTK && git checkout v{self.version}')

        if self.settings.compiler == 'gcc':
            import cmake_helpers
            cmake_helpers.wrapCMakeFile(os.path.join(self.source_folder, 'VTK'), output_func=self.output.info)

    def _set_up_cmake(self):

        cmake = CMake(self)

        if 'fPIC' in self.options and self.options.fPIC:
            cmake.definitions['CMAKE_POSITION_INDEPENDENT_CODE'] = 'ON'

        if self.settings.compiler == 'gcc':
            cmake.definitions['ADDITIONAL_CXX_FLAGS:STRING'] = '-frecord-gcc-switches'

        cmake.definitions['BUILD_SHARED_LIBS:BOOL'] = 'TRUE' if self.options.shared else 'FALSE'
        cmake.definitions['BUILD_TESTING:BOOL']     = 'OFF'

        if self.options.with_qt:
            cmake.definitions['VTK_QT_VERSION:STRING']             = '5'
            cmake.definitions['VTK_Group_Qt:BOOL']                 = 'OFF'
            cmake.definitions['Module_vtkGUISupportQt:BOOL']       = 'ON'
            cmake.definitions['Module_vtkGUISupportQtOpenGL:BOOL'] = 'ON'
            cmake.definitions['CMAKE_PREFIX_PATH:PATH']            = os.path.join(self.deps_cpp_info['qt'].rootpath, 'lib', 'cmake')

        # I placed there here to prevent timeouts
        cmake.definitions['ExternalData_TIMEOUT_INACTIVITY'] = 700
        cmake.definitions['ExternalData_TIMEOUT_ABSOLUTE']   = 700

        if self.settings.compiler == 'gcc' and Version(str(self.settings.compiler.version)) >= '5':
            # A problem I only got when I switched to 16.04
            # SO answer suggesting this: https://stackoverflow.com/a/28761703/1861346
            cmake.definitions['CMAKE_C_FLAGS:STRING']   = '-DGLX_GLXEXT_LEGACY'
            cmake.definitions['CMAKE_CXX_FLAGS:STRING'] = '-DGLX_GLXEXT_LEGACY'

        return cmake

    def build(self):

        cmake = self._set_up_cmake()

        # Debug
        s = '\nCMake Definitions:\n'
        for k,v in cmake.definitions.items():
            s += ' - %s=%s\n'%(k, v)
        self.output.info(s)

        cmake.configure(source_folder='VTK')
        cmake.build()
        cmake.install()

    def cmakeRelDir(self):
        """ Grab the directory VTK's cmake is in """
        vtk_major = '.'.join(self.version.split('.')[:2])
        return os.path.join('lib', 'cmake', f'vtk-{vtk_major}')

    def package(self):
        cmake = self._set_up_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.resdirs = [self.cmakeRelDir()]

# vim: ts=4 sw=4 expandtab ffs=unix ft=python :
