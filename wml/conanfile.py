import os
from conans import ConanFile, CMake, tools


class WmlConan(ConanFile):
    """
    Create our custom WML.

    Keep this script Python 2.7 compatible such that it can be run on the
    embedded OS
    """

    name            = 'wml'
    version         = '3.x'
    description     = 'WML'
    settings        = 'os', 'compiler', 'build_type', 'arch', 'arch_build'
    exports         = 'Wml3/*'
    options         = {
        'shared': [True, False],
        'fPIC':   [True, False],
    }
    default_options = 'shared=True', 'fPIC=True'
    no_copy_source  = True

    def config_options(self):
        if 'Windows' == self.settings.os or self.settings.arch == 'ppc64':
            # Windows:
            #   Wml attmpts to export some instantiated template static consts.
            #   These are not exportable.  These wither have to be converted to
            #   a function, or this lib simpler cannot be shared (I've chosen
            #   the latter after two days of trying to make this shared'able)
            # ppc64:
            #   We want to ensure that on the embedded PPC system that this is
            #   static.  This is likely a bad place to insist on it though..
            self.options.remove("shared")

        if self.settings.compiler == "Visual Studio":
            self.options.remove("fPIC")

    def source(self):
        self.run('git clone http://ntctrac.corp.neptec.com:9090/ntc_software/o2libs.git')
        self.run('cd o2libs')

    def build(self):
        cmake = CMake(self)
        cmake.definitions['BUILD_SHARED_LIBS:BOOL'] = 'ON' if 'shared' in self.options and self.options.shared else 'OFF'

        if 'fPIC' in self.options and self.options.fPIC:
            cmake.definitions['CMAKE_POSITION_INDEPENDENT_CODE:BOOL'] = 'ON'

        if not tools.cross_building(self.settings) and 'gcc' == self.settings.compiler and self.settings.arch_build in ['x86', 'x86_64']:
            cmake.definitions['CMAKE_CXX_FLAGS:STRING'] = '-mtune=generic'

        cmake.configure(source_folder=os.path.join('o2libs', 'wml3'))
        cmake.build()

    def package(self):
        if 'Linux' == self.settings.os:
            self.copy('*.a',  dst='lib', src=self.build_folder)
            self.copy('*.so', dst='lib', src=self.build_folder)
        else:
            self.copy('*.lib', dst='lib', keep_path=False)
            self.copy('*.dll', dst='bin', keep_path=False)
            self.copy('*.pdb', dst='bin', keep_path=False)
        for p in ['h', 'inl', 'mcr']:
            self.copy('*.%s'%p, dst='include', src=os.path.join(self.source_folder, 'o2libs', 'wml3', 'include'), keep_path=True)

    def package_info(self):
        self.cpp_info.libs = ['Wml3']

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
