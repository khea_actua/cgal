#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import os, shutil, glob
from conans import ConanFile, CMake, tools


class Tf2Conan(ConanFile):
    name             = 'tf2'
    version          = 'indigo'
    license          = 'Creative Commons Attribution 3.0'
    url              = 'http://wiki.ros.org/tf2'
    description      = 'tf2 is the second generation of the transform library, which lets the user keep track of multiple coordinate frames over time.'
    settings         = 'os', 'compiler', 'build_type', 'arch'
    options          = {'shared': [True, False]}
    default_options  = 'shared=True'
    generators       = 'cmake'
    md5sum           = '44d4884d16abbdac1c23462893da528e'
    requires = (
        'boost/[>1.46,<=1.60.0]@ntc/stable',
        'helpers/0.3@ntc/stable',
    )
    options = {
        'shared': [True, False],
    }
    default_options = ('shared=True')

    def config_options(self):
        if 'Visual Studio' == self.settings.compiler:
            # When shared, VS trips over a bunch of undeclared symbols
            self.options.remove('shared')

    def source(self):
        zip_name = 'tf2_neptec-indigo.tar.gz'
        from source_cache import copyFromCache
        if not copyFromCache(zip_name):
            self.output.error('This recipe relies on a pre-downloaded version of TF2.  Without this, please use the conan TF2 recipe at https://github.com/kheaactua/conan-tf2')
        tools.check_md5(zip_name, self.md5sum)
        tools.unzip(zip_name)
        os.rename(src='tf2_neptec', dst=self.name)
        os.unlink(zip_name) 

    def _setup_cmake(self):
        cmake = CMake(self)

        cmake.definitions['BOOST_ROOT:PATH']        = self.deps_cpp_info['boost'].rootpath
        cmake.definitions['BUILD_SHARED_LIBS:BOOL'] = 'TRUE' if 'shared' in self.options and self.options.shared else 'FALSE'

        return cmake

    def build(self):
        cmake = self._setup_cmake()
        cmake.configure(source_folder=self.name)
        cmake.build()

    def package(self):
        cmake = self._setup_cmake()
        cmake.configure(source_folder=self.name)
        cmake.install()

        if tools.os_info.is_windows:
            # While right now we don't permit shared builds in Windows, if the
            # missing symbol issue is ever fixed, then make sure that DLLs
            # are installed to bin/, not lib/
            # Note: console_bridge appears to build a DLL regardless of the
            #       shared option.
            dst = os.path.join(self.package_folder, 'bin')
            os.makedirs(dst)

            base = os.path.join(self.package_folder, 'lib')
            files = glob.glob(os.path.join(base, '*.dll'))
            for f in files:
                if os.path.exists(f):
                    shutil.move(src=f, dst=dst)

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
