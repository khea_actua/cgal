#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import os, shutil
from conans import ConanFile, CMake, tools
from conans.model.version import Version
# 18.04: https://github.com/carla-simulator/carla/issues/503

class CgalConan(ConanFile):
    """ Tested with versions 4.8.1, 4.8.2, and 4.10.2. 4.11, 4.13 """

    name         = 'cgal'
    license      = 'https://github.com/CGAL/cgal/blob/master/LICENSE.md'
    url          = 'https://www.cgal.org/'
    description  = 'The Computational Geometry Algorithms Library (CGAL) is a C++ library that aims to provide easy access to efficient and reliable algorithms in computational geometry.'
    settings     = 'os', 'compiler', 'build_type', 'arch', 'arch_build'
    generators   = 'cmake'
    requires     = (
        'gmp/[>=5.0.0]@ntc/stable',
        'mpfr/[>=4.0.0]@ntc/stable',
        'boost/[>1.46]@ntc/stable',
        'helpers/0.3@ntc/stable',
    )
    options = {
        'shared':        [True, False],
        'download_deps': [True, False],
        'with_qt':       [True, False],
    }
    default_options = ('shared=True', 'download_deps=False', 'with_qt=False')
    no_copy_source = True


    def config_options(self):
        if 'Visual Studio' == self.settings.compiler:
            # Currently (at least this version) doesn't work well as shared with VS
            self.options.remove('shared')

    def requirements(self):
        if self.options.with_qt:
            if Version(str(self.version)) > '4.12':
                self.requires('qt/[>=5.6.0]@ntc/stable')
            else:
                self.requires('qt/[>=5.3.2]@ntc/stable')

    def source(self):
        import platform_helpers

        ext = 'tar.xz' if tools.os_info.is_linux else 'zip'
        archive_url=f'https://github.com/CGAL/cgal/releases/download/releases%2FCGAL-{self.version}/CGAL-{self.version}.{ext}'
        ms5_url=f'https://github.com/CGAL/cgal/releases/download/releases%2FCGAL-{self.version}/md5sum.txt'

        # Avoiding tools.get because it only works with zip files
        archive = os.path.basename(archive_url)
        tools.download(url=archive_url, filename=archive)

        # Check the hash
        tools.download(url=ms5_url, filename='md5.txt')
        platform_helpers.check_hash(file_path=archive, hash_file='md5.txt', fnc=tools.check_md5)

        # Extract it
        if ext == 'tar.xz':
            self.run(f"tar xf {archive}")
        else:
            tools.unzip(archive)
        shutil.move(f'CGAL-{self.version}', self.name)

        # if on windows, get CGAL.  Note, a copy of MPFR and GMP also exist in
        # /shares/gary/Applications/cache/
        if tools.os_info.is_windows and self.options.download_deps:
            # Download GMP
            zipfile = 'gmp-all-CGAL-3.9.zip'
            if self.settings.arch == 'x86_64':
                arch = 'x64'
                md5_hash='508c1292319c832609329116a8234c9f'
            else:
                arch = 'win32'
                md5_hash='b2595894b6f80dac0743701aa2664116'

            # Note: This isn't well tested, as we've never taken this route
            tools.get(
                url=f'https://cgal.geometryfactory.com/CGAL/precompiled_libs/auxiliary/{arch}/GMP/5.0.1/{zipfile}',
                md5=md5_hash,
                destination=os.path.join(self.package_folder, 'auxiliary', 'gmp')
            )

    def build(self):
        cmake = CMake(self)

        # https://doc.cgal.org/latest/Manual/installation.html#sec3partysoftwareconfig
        cmake.definitions['BOOST_ROOT:PATH'] = self.deps_cpp_info['boost'].rootpath
        cmake.definitions['Boost_NO_SYSTEM_PATHS:BOOL'] = 'TRUE'

        cmake.definitions['BUILD_SHARED_LIBS:BOOL'] = 'TRUE' if 'shared' in self.options and self.options.shared else 'FALSE'
        if 'qt' in self.deps_cpp_info.deps:
            cmake.definitions['WITH_CGAL_Qt5:BOOL'] = 'TRUE'
            cmake.definitions['Qt5_DIR:PATH'] = os.path.join(self.deps_cpp_info['qt'].rootpath, 'lib', 'cmake', 'Qt5')
        else:
            cmake.definitions['WITH_CGAL_Qt5:BOOL'] = 'FALSE'

        def guessLib(name):
            if tools.os_info.is_windows:
                prefix = 'lib' # Because it was cross compiled, it's still a 'lib'
                suffix = 'dll.a' if self.options[name].shared else 'a'
            else:
                prefix = 'lib'
                suffix = 'so' if self.options[name].shared else 'a'

            lib = os.path.join(self.deps_cpp_info[name].rootpath, self.deps_cpp_info[name].libdirs[0], f'{prefix}{name}.{suffix}')
            return lib


        cmake.definitions['GMP_INCLUDE_DIR:PATH']   = ';'.join(self.deps_cpp_info['gmp'].include_paths)
        cmake.definitions['GMP_LIBRARIES_DIR:PATH'] = ';'.join(self.deps_cpp_info['gmp'].lib_paths)
        cmake.definitions['GMP_LIBRARIES:PATH']     = guessLib('gmp')

        cmake.definitions['MPFR_INCLUDE_DIR:PATH'] = ';'.join(self.deps_cpp_info['mpfr'].include_paths)
        cmake.definitions['MPFR_LIBRARIES:PATH']   = guessLib('mpfr')

        s = '\nCMake Definitions:\n'
        for k,v in cmake.definitions.items():
            s += ' - %s=%s\n'%(k, v)
        self.output.info(s)

        cmake.configure(source_folder=self.name)

        if 'qt' in self.deps_cpp_info.deps and tools.os_info.is_windows and 'x86' == self.settings.arch:
            # Fix the qtdatetime error
            # https://forum.qt.io/topic/39782/c2589-illegal-token-on-right-side-of/3
            file = os.path.join(self.build_folder, 'src', 'CGAL_Qt5', 'all_files.cpp')
            with open(file, 'r') as f: data = f.read()
            data = '#define NOMINMAX\n\n' + data
            with open(file, 'w') as f: f.write(data)

        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

        # For Windows, to reduce the impact on PATH (somehow it's 2019 and
        # that's still a problem), copy the GMP and MPFR dlls into CGAL's bin
        # directory.  We don't copy static libs as they shouldn't take up space
        # in PATH
        if tools.os_info.is_windows:
            for d in 'mpfr', 'gmp':
                for p in '*.dll', '*.pdb':
                    self.copy(p, src=os.path.join(self.deps_cpp_info[d].rootpath, self.deps_cpp_info[d].bindirs[0]), dst='bin')

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
