#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import shutil, os, re
from conans import ConanFile, CMake, tools
from conans.errors import ConanException


class AssimpConan(ConanFile):
    """ Tested with v4.1.0 and v3.3.1 """

    name            = 'assimp'
    license         = 'http://assimp.sourceforge.net/main_license.html'
    url             = 'http://assimp.sourceforge.net/'
    description     = 'Official Open Asset Import Library Repository. Loads 40+ 3D file formats into one unified and clean data structure.'
    settings        = 'os', 'compiler', 'build_type', 'arch', 'arch_build'
    options         = {'shared': [True, False]}
    default_options = 'shared=True'
    requires        = 'helpers/0.3@ntc/stable'

    def source(self):
        ext = 'tar.gz'
        archive=f'v{self.version}.{ext}'
        archive_url=f'https://github.com/assimp/assimp/archive/{archive}'

        hashes = {
            '4.1.0': '83b53a10c38d964bd1e69da0606e2727',
            '3.3.1': 'fc57b024e80ebb13301bd0983826cad3',
        }

        tools.download(url=archive_url, filename=archive)
        tools.check_md5(archive, hashes[self.version])

        tools.unzip(archive)
        shutil.move(f'assimp-{self.version}', self.name)

    def _set_up_cmake(self):
        """
        Set up the CMake generator so that it can be used in build() and package()
        """

        cmake = CMake(self)

        cmake.definitions['BUILD_SHARED_LIBS:BOOL']    = 'TRUE' if self.options.shared else 'FALSE'
        cmake.definitions['ASSIMP_BUILD_TESTS:BOOL']   = 'FALSE'
        cmake.definitions['ASSIMP_BUILD_SAMPLES:BOOL'] = 'FALSE'
        # TODO add Qt

        return cmake

    def build(self):

        cmake = self._set_up_cmake()

        cmake.configure(source_folder=self.name)
        cmake.build()

    def package(self):

        cmake = self._set_up_cmake()
        cmake.configure(source_folder=self.name)
        cmake.install()

        pkg_config_file = os.path.join(self.package_folder, 'lib', 'pkgconfig', 'assimp.pc')

        #
        # Fix up the pkg-config file before installing it.
        with open(pkg_config_file) as f: data = f.read()

        # The include path in the pkg-config file is a little off
        data = data.replace('include/assimp', 'include')

        from platform_helpers import adjustPath

        # Make sure it uses prefix rather than hard paths
        data = data.replace(adjustPath(self.package_folder), '${prefix}')

        # Fix ths exec path
        data = re.sub(r'(exec_prefix=\$\{prefix\}/)(\W)', r'\1bin\2', data)

        # On Windows, the lib name is something like assimp-vc120-mt.lib, so
        # easier to detect it.
        libs = tools.collect_libs(self)
        def libName(file_name):
            """ Format the lib name for cflags, but also ignore the non-assimp specific libs """
            if not re.search('assimp', file_name):
                return ''
            l = file_name.replace('.lib' if 'Windows' == self.settings.os else 'lib', '')
            return '-l%s'%l

        data = re.sub(r'-lassimp\b', ' '.join(map(libName, libs)), data)

        # Write out the corrected pkg-config file
        with open(pkg_config_file, 'w') as f: f.write(data)

        # Final check
        with open(pkg_config_file) as f: data = f.read()
        if re.search('exec_prefix=.*mrussell', data):
            raise ConanException('pkg-config file not properly fixed!')

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

        # Populate the pkg-config environment variables
        with tools.pythonpath(self):
            from platform_helpers import adjustPath, appendPkgConfigPath

            self.env_info.PKG_CONFIG_ASSIMP_PREFIX = adjustPath(self.package_folder)
            appendPkgConfigPath(adjustPath(os.path.join(self.package_folder, 'lib', 'pkgconfig')), self.env_info)

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
