#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import os, shutil
from conans import ConanFile, CMake, tools


class EigenConan(ConanFile):
    """ Tested with versions 3.3.4 and 3.2.9 """

    name = 'eigen'
    license = 'LGPL3+.'
    url = 'http://eigen.tuxfamily.org'
    settings = 'os', 'compiler', 'build_type', 'arch'
    requires = (
        'helpers/0.3@ntc/stable',
        # Note: This could accept Qt, GLUT (freeglut?), GMP, and MPFR
    )
    generators = 'cmake'

    def source(self):
        archive = 'eigen.tar.gz'
        tools.download(f'http://bitbucket.org/eigen/eigen/get/{self.version}.tar.gz', archive)
        tools.unzip(archive)

        hashes = {
            '3.3.7': {'v': '323c052e1731', 'h': 'f2a417d083fe8ca4b8ed2bc613d20f07'},
            '3.3.5': {'v': 'b3f3d4950030', 'h': 'ee48cafede2f51fe33984ff5c9f48026'},
            '3.3.4': {'v': '5a0156e40feb', 'h': '1a47e78efe365a97de0c022d127607c3'},
            '3.2.9': {'v': 'dc6cfdf9bcec', 'h': '6a578dba42d1c578d531ab5b6fa3f741'},
            '3.2.5': {'v': 'bdd17ee3b1b3', 'h': '8cc513ac6ec687117acadddfcacf551b'},
        }

        tools.check_md5(archive, hashes[self.version]['h'])

        shutil.move('eigen-eigen-%s'%hashes[self.version]['v'], self.name)

    def package_id(self):
        self.info.header_only()

    def build(self):

        extra_envs={}
        if tools.os_info.is_linux:
            extra_envs['FC'] = 'nofortran'

        cmake = CMake(self)
        with tools.environment_append(extra_envs):
            cmake.configure(source_folder=self.name)
            cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.configure(source_folder=self.name)
        cmake.install()

        # In 3.2, a hard path is placed into the pkg-config file
        eigen_major = int(self.version.split('.')[1])
        if eigen_major < 3:
            self.fixPkgConfig(os.path.join(self.package_folder, 'share', 'pkgconfig', 'eigen3.pc'))

    def package_info(self):
        self.cpp_info.includedirs = [os.path.join('include', 'eigen3')]

        # Populate the pkg-config environment variables
        with tools.pythonpath(self):
            from platform_helpers import adjustPath, appendPkgConfigPath

            # I don't think PKG_CONFIG_EIGEN_PREFIX is actually used
            self.env_info.PKG_CONFIG_EIGEN_PREFIX = adjustPath(self.package_folder)
            self.env_info.PKG_CONFIG_EIGEN3_PREFIX = adjustPath(self.package_folder)
            appendPkgConfigPath(adjustPath(os.path.join(self.package_folder, 'share', 'pkgconfig')), self.env_info)

    def fixPkgConfig(self, eigen_pc):
        # pkg config files don't seem to be generated on windows by default.
        if self.settings.os == "Linux":
            self.output.info('Modifying pkg-config file to start with a prefix')
            with open(eigen_pc) as f: data = f.read()
            data = 'prefix=%s\nexec_prefix=${prefix}\n\n'%self.package_folder + data
            with open(eigen_pc, 'w') as f: f.write(data)

            self.output.info('Modifying pkg-config file to use a variable prefix')
            tools.replace_in_file(
                file_path=eigen_pc,
                search=self.package_folder,
                replace="${prefix}"
            )

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
