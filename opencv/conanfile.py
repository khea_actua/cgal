#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import shutil, os, re, platform
from conans import ConanFile, CMake, tools
from conans.errors import ConanException


class OpenCVConan(ConanFile):
    """ Tested with versions 2.4.9, 3.1.0, and 3.4.4 """

    name        = 'opencv'
    license     = 'https://github.com/opencv/opencv/blob/master/LICENSE'
    description = 'OpenCV (Open Source Computer Vision Library) is released under a BSD license and hence it’s free for both academic and commercial use.'
    url         = 'http://opencv.org'
    settings    = 'os', 'compiler', 'build_type', 'arch'
    generators  = 'cmake'
    options     = {
        'shared':   [True, False],
        'with_vtk': [True, False],
    }
    default_options = 'shared=True', 'with_vtk=True'

    requires = (
        'zlib/[>=1.2.11]@conan/stable',
        'eigen/[>=3.2.0]@ntc/stable',
        'helpers/0.3@ntc/stable',
    )

    opencv_libs = [
        'contrib','stitching', 'nonfree', 'superres', 'ocl', 'ts',
        'videostab', 'gpu', 'photo', 'objdetect', 'legacy', 'video',
        'ml', 'calib3d', 'features2d', 'highgui', 'imgproc', 'core',
        'flann'
    ]

    def system_requirements(self):
        pack_names = None
        if tools.os_info.linux_distro == "ubuntu":
            pack_names = ['libv4l-0']

            if self.settings.arch == "x86":
                full_pack_names = []
                for pack_name in pack_names:
                    full_pack_names += [pack_name + ":i386"]
                pack_names = full_pack_names

        if pack_names:
            installer = tools.SystemPackageTool()
            try:
                installer.update() # Update the package database
                installer.install(" ".join(pack_names)) # Install the package
            except ConanException:
                self.output.warn('Could not run system updates to fetch build requirements.')

    def configure(self):
        if self.options.with_vtk:
            self.requires('vtk/[>=5.6.1]@ntc/stable')

    def source(self):
        archive             = f'{self.version}'
        archive_file        = f'{archive}.tar.gz'
        cached_archive_file = f'opencv-{archive_file}'

        from source_cache import copyFromCache
        if not copyFromCache(f'opencv-{self.version}.tar.gz'):
            hashes = {
                '3.4.4':    'd4b03579405a69ef765e4c79f42c7c82',
                '3.4.3':    '712896f5815938c014c199dde142d508',
                '3.4.0':    '170732dc760e5f7ddeccbe53ba5d16a6',
                '3.1.0':    'a0669e22172dfc3225835b180744c9f0',
                '2.4.13.6': 'ba2a2af6c0737da9dc304ba622a8577f',
                '2.4.9':    'cc0a8307403ff471f554197401ec0eb9',
            }

            archive_url=f'https://github.com/opencv/opencv/archive/{self.version}.tar.gz'
            tools.download(url=archive_url, filename=cached_archive_file)
            tools.check_md5(cached_archive_file, hashes[self.version])

        tools.unzip(cached_archive_file)
        shutil.move(f'opencv-{self.version}', self.name)
        os.unlink(f'opencv-{self.version}.tar.gz')

        self.fixFindEigen()


    def fixFindEigen(self):
        """
        The OpenCV code to find Eigen is annoying, virutally unusable.  This
        function fixes it by adding an environment variable into its find_path
        """

        find_eigen_file = f'{self.name}/cmake/OpenCVFindLibsPerf.cmake'

        # Now, run some regex's through the
        with open(find_eigen_file) as f: data = f.read()

        regex = r'PATHS(?P<whitespace>\s+)(?P<paths>.*((ENV.EIGEN_ROOT.)|(ENV ProgramFiles)))'
        m = re.search(regex, data)
        if m:
            data = data.replace(
                m.group(0),
                'PATHS%s$ENV{CONAN_EIGEN_INCLUDE_DIR} %s'%(m.group('whitespace'), m.group('paths'))
            )
        else:
            self.output.warn('Could not insert environment variable to help find Eigen in %s'%find_eigen_file)

        with open(find_eigen_file, 'w') as f: f.write(data)


    def build(self):


        cmake = CMake(self)
        cmake.definitions['BUILD_SHARED_LIBS:BOOL']       = 'TRUE' if self.options.shared else 'FALSE'
        cmake.definitions['BUILD_EXAMPLES:BOOL']          = 'OFF'
        cmake.definitions['INSTALL_C_EXAMPLES:BOOL']      = 'OFF'
        cmake.definitions['INSTALL_PYTHON_EXAMPLES:BOOL'] = 'OFF'
        cmake.definitions['WITH_CUDA:BOOL']               = 'OFF'
        cmake.definitions['BUILD_DOCS:BOOL']              = 'OFF'
        cmake.definitions['BUILD_TESTS:BOOL']             = 'OFF'
        cmake.definitions['BUILD_opencv_apps:BOOL']       = 'OFF'
        cmake.definitions['BUILD_PERF_TESTS:BOOL']        = 'OFF'

        cmake.definitions['ZLIB_INCLUDE_DIR'] = os.path.join(self.deps_cpp_info['zlib'].rootpath, 'include')
        if self.options['zlib'].shared:
            libz = 'libz.so' if self.settings.os == 'Linux' else 'libz.dll'

        else:
            libz = 'libz.a' if self.settings.os == 'Linux' else 'libz.lib'
        cmake.definitions['ZLIB_LIBRARY_RELEASE'] = os.path.join(self.deps_cpp_info['zlib'].rootpath, 'lib', libz)

        if 'vtk' in self.deps_cpp_info.deps:
            vtk_major = '.'.join(self.deps_cpp_info['vtk'].version.split('.')[:2])
            cmake.definitions['VTK_DIR:PATH'] = os.path.join(self.deps_cpp_info['vtk'].rootpath, 'lib', 'cmake', f'vtk-{vtk_major}')
            cmake.definitions['WITH_VTK:BOOL'] = 'ON'
        else:
            cmake.definitions['WITH_VTK:BOOL'] = 'OFF'

        if 'Visual Studio' == self.settings.compiler:
            cmake.definitions['BUILD_WITH_STATIC_CRT:BOOL'] = 'ON' if 'MT' in str(self.settings.compiler.runtime) else 'OFF'

        env_vars = {
            'CONAN_EIGEN_INCLUDE_DIR': os.path.join(self.deps_cpp_info['eigen'].rootpath, self.deps_cpp_info['eigen'].includedirs[0]),
        }
        with tools.environment_append(env_vars):
            cmake.configure(source_folder=self.name)
            cmake.build()

        cmake.install()

    def package(self):
        pass

    def package_info(self):
        # Add the directory with CMake.. Not sure if this is a good use of resdirs
        if 'Windows' == platform.system():
            # On Windows, this CMake file is in the root of the package folder
            self.cpp_info.resdirs = [self.package_folder]

            if 'Visual Studio' == self.settings.compiler:
                short_comp = 'vc%s'%self.settings.compiler.version
            else:
                raise ConanException(f'Compiler "{self.settings.compiler}" not yet implemented')
            short_arch = 'x64' if 'x86_64' == self.settings.arch else 'x86'
            self.cpp_info.libdirs = [os.path.join(self.package_folder, short_arch, short_comp, 'lib')]
            self.cpp_info.bindirs = [os.path.join(self.package_folder, short_arch, short_comp, 'bin')]

            self.output.info('Lib dirs: %s'%' '.join(self.cpp_info.libdirs))
            self.output.info('Bin dirs: %s'%' '.join(self.cpp_info.bindirs))
        else:
            self.cpp_info.resdirs = [os.path.join(self.package_folder, 'share', 'OpenCV')]

        self.cpp_info.libs = tools.collect_libs(self, folder=self.cpp_info.libdirs[0])
        self.output.info('Libs   : %s'%' '.join(self.cpp_info.libs))

        # Populate the pkg-config environment variables
        with tools.pythonpath(self):
            from platform_helpers import adjustPath, appendPkgConfigPath

            self.env_info.PKG_CONFIG_OPENCV_PREFIX = adjustPath(self.package_folder)
            appendPkgConfigPath(adjustPath(os.path.join(self.package_folder, 'lib', 'pkgconfig')), self.env_info)

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
