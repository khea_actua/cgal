#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

from conans import ConanFile


class IcpbaseConan(ConanFile):
    name = 'icpbase'
    version = '0.5'
    license = 'Neptec Technologies Corporation'
    description = 'Iterative point library base'
    exports = 'IcpBase/*'

    def source(self):
        pass

    def build(self):
        pass

    def package(self):
        self.copy(pattern='*.h', dst='include/IcpBase', src='IcpBase')

    def package_info(self):
        pass

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
