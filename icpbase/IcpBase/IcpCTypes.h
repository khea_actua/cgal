/*
 Copyright (c) Neptec Design Group Limited, 2013. All rights reserved.

 $Id $
 $HeadURL$
 $LastChangedBy$
 $LastChangedDate$
 $LastChangedRevision$
*/

#ifndef ICP_CTYPES_H
#define ICP_CTYPES_H

#ifdef WIN32
#define DllExport __declspec( dllexport )
#else
#define DllExport
#endif

/* Define bool if this is included by a file compiled in C. */
#ifndef __cplusplus
typedef enum {false, true} bool;
#endif 

struct Vector3_t
{
    float X;
    float Y;
    float Z;
};

struct BoundingSphere_t
{
    Vector3_t center;
    float radius;
};

struct BoundingBox_t
{
    Vector3_t center;
    Vector3_t axis[3];
    float extent[3];
};

struct RangeImage_t
{
    bool perspective; /* set to true if perspective, false if orthographic */
    unsigned int width;
    unsigned int height;
    Vector3_t* data; /* range image data in row major format. */
};

struct TransformationMatrix_t
{
    bool bRowMajor;
    float pose[12]; /* first three row of the 4x4 tranformation matrix */
};

struct Matrix3_t
{
    bool bRowMajor;
    float data[9];
};

#endif /* ICP_TYPES_H */

