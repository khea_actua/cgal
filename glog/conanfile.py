#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import os, shutil
from conans import ConanFile, CMake, tools


class GlogConan(ConanFile):
    name            = 'glog'
    # version       = '0.3.5'
    license         = ''
    url             = 'https://github.com/google/glog'
    description     = 'C++ implementation of the Google logging module'
    settings        = 'os', 'compiler', 'build_type', 'arch'
    options         = {'shared': [True, False]}
    default_options = 'shared=True'

    def source(self):
        hashes = {
            '0.3.5': '5df6d78b81e51b90ac0ecd7ed932b0d4',
        }

        if self.version in hashes:
            archive = f'v{self.version}.tar.gz'
            tools.download(
                url=f'https://github.com/google/glog/archive/{archive}',
                filename=archive
            )
            tools.check_md5(archive, hashes[self.version])
            tools.unzip(archive)
            shutil.move(f'glog-{self.version}', self.name)
        else:
            self.run(f'git clone https://github.com/mariusmuja/flann {self.name}')
            self.run(f'cd {self.name} && git checkout v{self.version}')

    def build(self):
        cmake = CMake(self)
        cmake.definitions['BUILD_SHARED_LIBS:BOOL'] = 'TRUE' if self.options.shared else 'FALSE'

        cmake.configure(source_folder=self.name)
        cmake.build()
        cmake.install()

    def package(self):
        pass

    def package_info(self):
        self.cpp_info.resdirs.append(os.path.join('lib', 'cmake'))
        self.cpp_info.libs = tools.collect_libs(self)

        if 'Windows' == self.settings.os:
            self.env_info.path.append(os.path.join(self.package_folder, "bin"))

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
