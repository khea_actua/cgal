#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import os, shutil
from conans import ConanFile, tools, AutoToolsBuildEnvironment


class NtbtlsConan(ConanFile):
    name            = 'ntbtls'
    version         = '0.1.2'
    md5_hash        = '81490fbbe551768019689071443f4e36'
    license         = 'Creative Commons Attribution-ShareAlike 3.0 Unported'
    url             = 'https://www.gnupg.org/download/index.en.html'
    description     = 'NTBTLS is the standard library to access GnuPG functions from programming languages.'
    settings        = 'os', 'compiler', 'build_type', 'arch'
    requires        = (
        'gpg_error/1.30@ntc/stable',
        'ksba/1.3.5@ntc/stable',
        'gcrypt/1.8.2@ntc/stable',
        'zlib/1.2.11@conan/stable',
    )
    options = {'deploy_prefix': 'ANY'}
    default_options = (
        'deploy_prefix=/opt/gnupg'
    )

    def source(self):
        url = f'https://www.gnupg.org/ftp/gcrypt/ntbtls/ntbtls-{self.version}.tar.bz2'
        archive = os.path.basename(url)
        tools.download(url=url, filename=archive)

        # Ironically, I don't know how to use the .sig file, so we use the md5hash
        tools.check_md5(archive, self.md5_hash)

        tools.unzip(archive)
        shutil.move(f'ntbtls-{self.version}', self.name)

    def build(self):

        args = []
        if 'TARGETMACH' in os.environ:
            args.append('--host=%s'%os.environ['TARGETMACH'])

        args.append(f'--prefix={self.package_folder}')
        args.append('--with-libgpg-error-prefix=%s'%self.deps_cpp_info['gpg_error'].rootpath)
        args.append('--with-libgcrypt-prefix=%s'%self.deps_cpp_info['gcrypt'].rootpath)
        args.append('--with-ksba-prefix=%s'%self.deps_cpp_info['ksba'].rootpath)
        args.append('--with-zlib-prefix=%s'%self.deps_cpp_info['zlib'].rootpath)

        autotools = AutoToolsBuildEnvironment(self)
        with tools.chdir(self.name):
            autotools.configure(args=args)
            autotools.make()

    def package(self):
        autotools = AutoToolsBuildEnvironment(self)
        with tools.chdir(self.name):
            autotools.make(args=['install'])

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

    def deploy(self):
        try:
            for d in self.cpp_info.libdirs:
                self.copy(pattern='*', src=os.path.join(self.package_folder, d), dst=os.path.join(str(self.options.deploy_prefix), d), symlinks=True)
        except PermissionError:
            self.output.warn("Could not copy libraries to %s."%str(self.options.deploy_prefix))

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
