#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import sys, shutil, os, platform
from conans import ConanFile, tools, AutoToolsBuildEnvironment


class AnnConan(ConanFile):
    name = 'ann'
    version = '1.1.2'
    license = 'GNU Lesser Public License.'
    url = 'https://www.cs.umd.edu/~mount/ANN/'
    description = 'A Library for Approximate Nearest Neighbour Searching'
    settings = 'os', 'compiler', 'build_type', 'arch'

    def source(self):

        tools.get(url=f'https://www.cs.umd.edu/~mount/ANN/Files/{self.version}/ann_{self.version}.zip', md5='31267ffbe4e6d04768b3ec21763e9343')
        shutil.move(f'ann_{self.version}', self.name)

    @property
    def linux_build_folder(self):
        return os.path.join(self.build_folder, self.name)

    def build(self):

        if self.settings.os == 'Linux':
            with tools.chdir(self.linux_build_folder):
                env_build = AutoToolsBuildEnvironment(self, win_bash = ('Windows' == platform.system()))

                env_build.cxx_flags.append('-fPIC')
                env_build.make(args=['linux-g++'])
        else:
            self.output.error('Not yet implemented')
            # There's a MS_Win32 directory with a solution file
            sys.exit(-1)

    def package(self):
        self.copy(pattern='*.a', dst='lib',     src=os.path.join(self.linux_build_folder, 'lib'))
        self.copy(pattern='*.h', dst='include', src=os.path.join(self.linux_build_folder, 'include'))
        self.copy(pattern='*',   dst='bin',     src=os.path.join(self.linux_build_folder, 'bin'))

    def package_info(self):
        self.cpp_info.libs = ['ANN']

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
