#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import sys, shutil, os
from conans import ConanFile, CMake, tools


class FreeglutConan(ConanFile):
    name            = 'freeglut'
    version         = '3.0.0'
    md5hash         = '90c3ca4dd9d51cf32276bc5344ec9754'
    license         = 'X-Consortium'
    url             = 'http://freeglut.sourceforge.net/'
    description     = 'FreeGLUT is a free-software/open-source alternative to the OpenGL Utility Toolkit (GLUT) library.'
    settings        = 'os', 'compiler', 'build_type', 'arch', 'arch_build'
    options         = {'shared': [True, False]}
    default_options = 'shared=True'
    generators      = 'cmake'

    def source(self):

        archive = f'freeglut-{self.version}.tar.gz'
        url = f'http://prdownloads.sourceforge.net/freeglut/{archive}?download'

        tools.download(url=url, filename=archive)
        tools.check_md5(archive, self.md5hash)

        tools.unzip(archive)
        shutil.move(f'freeglut-{self.version}', self.name)

    def build(self):

        if 'Linux' == self.settings.os:
            if not os.path.exists('/usr/include/X11/extensions/XInput.h'):
                self.output.error('Error: XInput.h is not available.  FreeGlut depends on this file.  Please install the inputplug packate with apt-get, and then symlink /usr/include/X11/extensions/XI.h to /usr/include/X11/extensions/XInput.h.  i.e.\n   sudo apt-get install inputplug && cd /usr/include/X11/extensions && sudo ln -s XI.h XInput.h\n\nThis was recommended here: http://melp.nl/2013/11/building-opengl-sdk-compile-error-x11-extensions-xinput-h/')
                sys.exit(-1)

        cmake = CMake(self)

        if 'Windows' == self.settings.os and 'Debug' == self.settings.build_type and self.options.shared:
            # No clue why, but freeglut_staticd.pdb just doesn't get build, so
            # shutting off static libs for now.
            cmake.definitions['FREEGLUT_BUILD_STATIC_LIBS'] = 'OFF'

        cmake.configure(source_folder=self.name)
        cmake.build()

    def package(self):
        cmake = CMake(self)
        if 'Windows' == self.settings.os and 'Debug' == self.settings.build_type:
            # Not sure why, but freeglut_static.pdb doesn't get built, which
            # fails the install
            cmake.definitions['INSTALL_PDB'] = 'OFF'
        cmake.configure(source_folder=self.name, build_folder=self.build_folder)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = list(set(tools.collect_libs(self)))

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
