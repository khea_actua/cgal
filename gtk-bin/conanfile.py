#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, glob, re
from conans import ConanFile, tools
from conans.model.version import Version


class GtkConan(ConanFile):
    name        = 'gtk'
    license     = 'GTK+ is free software and part of the GNU Project. However, the licensing terms for GTK+, the GNU LGPL, allow it to be used by all developers, including those developing proprietary software, without any license fees or royalties.'
    description = 'GTK+, or the GIMP Toolkit, is a multi-platform toolkit for creating graphical user interfaces. Offering a complete set of widgets, GTK+ is suitable for projects ranging from small one-off tools to complete application suites.  Note: Maybe this should be re-written to follow the instructions at https://www.gtk.org/download/windows.php'
    requires    = 'helpers/[>=0.3]@ntc/stable'

    # Limit how this package can be used
    settings = {
        'os':         ['Windows'],
        'arch':       ['x86_64', 'x86'],
        'build_type': ['Release'],
        'compiler':   None,
    }

    def source(self):
        url='http://win32builder.gnome.org/gtk+-bundle_{version}-{date}_{arch}.zip'
        arch = 'win64' if 'x86_64' == self.settings.arch else 'win32'

        hashes = {
            '3.10.4-win64': {'hash': '0ecd3896af3ba7bd836883f40404e39d', 'date': '20131202'},
            '3.10.4-win32': {'hash': '520bed70943974efdaeea2a1dbe48f84', 'date': '20131202'},

            '3.8.2-win64':  {'hash': '3277c363442b454f6137c170b7527bf1', 'date': '20131201'},
            '3.8.2-win32':  {'hash': '3f9b159207edf44937f209b4a5e6bb63', 'date': '20131001'},

            '3.6.4-win64':  {'hash': '78d4a777a0fc615016d2ba3483a8c6f9', 'date': '20131201'},
            '3.6.4-win32':  {'hash': '3277c363442b454f6137c170b7527bf1', 'date': '20130921'},

            '3.4.2-win64':  {'hash': 'e0d4f064ff1e5c8033b549d0a0813deb', 'date': '20131201'},
            '3.4.2-win32':  {'hash': 'fcaf70239cd23ba0eb28070bd3d1de54', 'date': '20131201'},
        }

        md5_hash = hashes[f'{self.version}-{arch}']['hash']
        date     = hashes[f'{self.version}-{arch}']['date']
        url      = url.format(date=date, arch=arch, version=self.version)

        from source_cache import copyFromCache
        if copyFromCache(os.path.basename(url)):
            tools.unzip(os.path.basename(url)) # unzip right into .
            os.remove(os.path.basename(url))
        else:
            tools.download(url=url, filename='gtk.zip')
            tools.check_md5('gtk.zip', md5_hash)
            tools.unzip('gtk.zip') # unzip right into .
            os.remove('gtk.zip')

        
        if tools.os_info.is_windows and 'Visual Studio' == self.settings.compiler and Version(str(self.settings.compiler.version)) <= '12':
            # The spacing makes tweaking this file super annoying, so using regexes
            config_file = os.path.join(self.source_folder, 'lib', 'glib-2.0', 'include', 'glibconfig.h')
            with open(config_file) as f: data = f.read()
            for m in 'G_HAVE_INLINE', 'G_CAN_INLINE':
                // This mightn't actually be running on multiline
                data = re.sub('#define\s+(%s)\s+1'%m, r'// #define \1 0', data, flags=re.MULTILINE)
            with open(config_file, 'w') as f: f.write(data)

    def package(self):
        self.copy('*')

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

        # Populate the pkg-config environment variables
        with tools.pythonpath(self):
            from platform_helpers import adjustPath, appendPkgConfigPath

            pkg_config_path = os.path.join(self.package_folder, 'lib', 'pkgconfig')
            appendPkgConfigPath(adjustPath(pkg_config_path), self.env_info)

            pc_files = glob.glob(adjustPath(os.path.join(pkg_config_path, '*.pc')))
            for f in pc_files:
                p_name = re.sub(r'\.pc$', '', os.path.basename(f))
                p_name = re.sub(r'\W', '_', p_name.upper())
                setattr(self.env_info, f'PKG_CONFIG_{p_name}_PREFIX', adjustPath(self.package_folder))

            appendPkgConfigPath(adjustPath(pkg_config_path), self.env_info)

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
