#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from conans import ConanFile, tools

class ViewdatabaseConan(ConanFile):
    """
    Source for ViewDatabase is closed.  Get a copy of the execuatable, and
    place it in lib/ (adjacent to include/) and create the pacakge with

    # $ conan export-pkg <path> 'view_database/1.0@ntc/stable' -f

    """

    name     = 'view_database'
    version  = '1.0'
    settings = 'os', 'compiler', 'arch'
    license  = 'Neptec Design Group'
    requires = ('icpbase/[>= 0.5]@ntc/stable')
    no_copy_source = True

    def source(self):
        g = tools.Git()
        g.clone('http://ntctrac.corp.neptec.com:9090/ntc_software/o2libs.git')

    def package(self):
        base = os.path.join(self.source_folder, 'view_database')
        self.copy(pattern='*', dst='include', src=os.path.join(base, 'include'))

        path = os.path.join(base, '%s', str(self.settings.os), str(self.settings.arch), str(self.settings.compiler), str(self.settings.compiler.version))
        self.copy(pattern='*', dst='lib', src=path%'lib')
        self.copy(pattern='*', dst='bin', src=path%'bin')

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
