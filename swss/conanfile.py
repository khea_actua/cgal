#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from conans import ConanFile, CMake, tools


class SwssConan(ConanFile):
    name        = 'swss'
    version     = '1.3.1'
    license     = 'https://gitlab.com/eidheim/Simple-WebSocket-Server/blob/master/LICENSE'
    url         = 'https://gitlab.com/eidheim/Simple-WebSocket-Server'
    description = 'A very simple, fast, multithreaded, platform independent WebSocket (WS) and WebSocket Secure (WSS) server and client library implemented using C++11, Boost.Asio and OpenSSL. Created to be an easy way to make WebSocket endpoints in C++.'
    settings    = 'os', 'compiler', 'build_type', 'arch'
    requires = (
        'boost/[>1.46]@ntc/stable',
        'OpenSSL/1.0.2n@conan/stable',
    )
    generators = 'cmake'

    def source(self):
        g = tools.Git(self.name)
        g.clone('https://gitlab.com/eidheim/Simple-WebSocket-Server.git', branch='v%s'%self.version)

        if 'Visual Studio' == self.settings.compiler:
            # -Wextra gets replaced with /Wextra which is an error to MSVC
            tools.replace_in_file(
                file_path=os.path.join(self.name, 'CMakeLists.txt'),
                search=' -Wextra',
                replace='',
                strict=False,
            )

    def build(self):
        cmake = CMake(self)
        cmake.definitions['BOOST_ROOT:PATH'] = self.deps_cpp_info['boost'].rootpath

        def libName(lib):
            prefix = ''
            if tools.os_info.is_linux:
                prefix = 'lib'
                ext = 'so' if self.options['OpenSSL'].shared else 'a'
            else:
                ext = 'lib'

            return '%s%s.%s'%(prefix, lib, ext)

        cmake.definitions['OPENSSL_INCLUDE_DIR:PATH']    = os.path.join(self.deps_cpp_info['OpenSSL'].rootpath, self.deps_cpp_info['OpenSSL'].includedirs[0])
        cmake.definitions['OPENSSL_CRYPTO_LIBRARY:PATH'] = os.path.join(self.deps_cpp_info['OpenSSL'].rootpath, self.deps_cpp_info['OpenSSL'].libdirs[0], libName('crypto'))
        cmake.definitions['OPENSSL_SSL_LIBRARY:PATH']    = os.path.join(self.deps_cpp_info['OpenSSL'].rootpath, self.deps_cpp_info['OpenSSL'].libdirs[0], libName('ssl'))

        # Debug
        s = '\nCMake Definitions:\n'
        for k,v in cmake.definitions.items():
            s += ' - %s=%s\n'%(k, v)
        self.output.info(s)

        cmake.configure(source_folder=self.name)
        cmake.build()

    def package(self):
        self.copy(pattern='*.hpp',     dst='include/swss', src=os.path.join(self.source_folder, self.name))
        self.copy(pattern='README.md', dst='',             src=os.path.join(self.source_folder, self.name))
        self.copy(pattern='LICENSE',   dst='',             src=os.path.join(self.source_folder, self.name))

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
