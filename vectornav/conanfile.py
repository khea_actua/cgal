#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import shutil, os
from conans import ConanFile, tools


class VectornavConan(ConanFile):
    name        = 'vectornav'
    version     = '0.3.2'
    license     = 'MIT'
    url         = 'https://www.vectornav.com/support/downloads'
    description = 'VectorNav Technologies is the leading innovator and manufacturer of embedded navigation solutions using the latest in MEMS inertial sensor and GPS/GNSS technology:  Legacy libraries no longer under active development'
    settings    = 'os'
    exports     = '*.patch'
    requires    = ('helpers/[>=0.3]@ntc/stable')

    def source(self):
        archive='vnccpplib-0-3-2'
        ext='tar.gz'

        from source_cache import copyFromCache
        if not copyFromCache(f'{archive}.{ext}'):
            # https://www.vectornav.com/support/downloads
            tools.download(
                url=f'https://www.vectornav.com/Downloads/Legacy/{archive}.{ext}',
                filename=f'{archive}.{ext}'
            )
            tools.check_md5(f'{archive}.{ext}', '6b3b937ad4b6e40bcc09b443d891f722')

        # Something is wrong with this tar.gz file, so have to ungunzip and
        # untar it in several steps
        self.run(f'%s {archive}.{ext}'%('gunzip' if tools.os_info.is_linux else 'gzip -d'))
        self.run(f'tar xf {archive}.tar')

        archive = archive.replace('-'.join(self.version.split('.')), self.version)
        try:
            shutil.move(archive, self.name)
        except FileNotFoundError:
            self.output.warn(f'Could find {archive}')

    def build(self):
        # Apply:
        # - Put an ifdef around code intended for the PPC
        # - Local modifications we seem to have in the 3DRi vendor directory
        # - Modify the include path to include VectorNav

        tools.patch(
            patch_file='ppc.patch',
            base_path=os.path.join(self.build_folder, self.name)
        )

        tools.patch(
            patch_file='ntc.patch',
            base_path=os.path.join(self.build_folder, self.name)
        )

    def package(self):
        if tools.os_info.is_windows:
            exclude='*linux*'
        else:
            exclude='*win32*'

        self.copy(pattern='*.h', dst=os.path.join('include', 'VectorNav'), src=os.path.join(self.name, 'include'), excludes=exclude)
        self.copy(pattern='*.c', dst='src', src=os.path.join(self.name, 'src'), excludes=exclude)

        if tools.os_info.is_windows:
            self.copy(pattern='*.dll', dst='bin', src=os.path.join(self.source_folder, self.name, 'lib'), keep_path=False)

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
