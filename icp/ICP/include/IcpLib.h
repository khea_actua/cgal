/*
 Copyright (c) Neptec Design Group Limited, 2013. All rights reserved.

 $Id $
 $HeadURL$
 $LastChangedBy$
 $LastChangedDate$
 $LastChangedRevision$
*/

#ifndef ICP_LIB_H
#define ICP_LIB_H

#include "IcpBase/IcpCTypes.h"

#ifdef __cplusplus
extern "C"
{
#endif
    struct IcpParameters_t
    {
        /* Flag indicating if automatic sub sampling will be applied. */
        bool m_bAutoSubSample;

        /* Number of points that the auto sub sample will try to obtain. */
        unsigned int m_uiTargetNumPoints;

        /* User defined sub sampling factor (not used if in auto mode). */
        unsigned int m_uiSubSample;

        /* Minimization window. */
        unsigned int m_uiMinimizationWindow;

        /* Maximum number of ICP iterations. */
        unsigned int m_uiMaxIterations;

        /* Maximum number of ICP point iterations. */
        unsigned int m_uiMaxPointIterations;

        /* Threshold on the fit error that will stop iterations. */
        float m_fFitMetricThreshold;

        /* Minimum distance for outlier rejection. */
        float m_fOutlierMinDistance;

        /* Dynamic outlier rejection scaling factor. This will multiply the ICP
           error standard deviation to yield new outlier distance threshold each
           iteration. Set to 0 to use a fixed value. */
        float m_fDynOutlierMultFactor;

        /* Flag indicating if a pre alignment will be performed. */
        bool m_bPreAlignment;

        /* Number of prealignment steps in X. */
        unsigned int m_uiNumStepsX;

        /* Pre alignment step size in X. */
        float m_fStepSizeX;

        /* Number of prealignment steps in Y. */
        unsigned int m_uiNumStepsY;

        /* Pre alignment step size in Y. */
        float m_fStepSizeY;

        /* Number of prealignment steps in Roll. */
        unsigned int m_uiNumStepsRoll;

        /* Pre alignment step size in Roll. */
        float m_fStepSizeRoll;

        /* Initial guess to the ICP (SCC'). */
        TransformationMatrix_t m_mInitialTransform;

        /* Centroid of the base image used for prealignment. */
        Vector3_t m_vBaseImgCentroid;
    };

    struct IcpResults_t
    {
        /* Flag indicating if ICP succeeded. */
        bool m_bSuccess;

        /* Final ICP transform in the camera frame, SCC'. */
        TransformationMatrix_t m_mFinalTransform;

        /* Final residuals. */
        float m_fFinalFitMetric;

        /* Fraction of scan points that were matched to the base image. */
        float m_fFractionMatchedPoints;

        /* Number of ICP iterations performed. */
        int m_iIterations;

        /* Flag indicating if the iterations stopped because not enough points
           matched. */
        bool m_bMatchedCheck;

        /* Flag indicating if the iterations stopped because the fit metric was
           smaller than the specified fit threshold. */
        bool m_bFitCheck;

        /* Flag indicating if the iterations stopped because the output did not
           change in the specified number of iterations window. */
        bool m_bWinCheck;

        /* Flag indicating if the iterations stopped because it reached the maximum
           number of iterations. */
        bool m_bItsCheck;

        /* Flag indicating if the iterations stopped because the transformation did
           not change more than the specified transform threshold. */
        bool m_bTransCheck;
    };

    bool DllExport ICP_Fit(IcpParameters_t params,
                           RangeImage_t* baseRI,
                           RangeImage_t* movingRI,
                           IcpResults_t* results);
#ifdef __cplusplus
}
#endif

#endif /* ICP_LIB_H */
