#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import os
from conans import ConanFile


class IcpConan(ConanFile):
    name        = 'icp'
    version     = '0.5'
    license     = 'Neptec Technologies Corporation'
    description = 'Iterative point library'
    exports     = 'ICP/*'
    requires    = ('icpbase/[>=0.5]@ntc/stable')

    def source(self):
        pass

    def build(self):
        pass

    def package(self):
        self.copy(pattern='*.h', dst='include/ICP', src='ICP', keep_path=False)

    def package_info(self):
        pass

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
