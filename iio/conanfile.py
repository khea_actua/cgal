#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from conans import ConanFile, CMake, tools


class IioConan(ConanFile):
    name            = 'iio'
    license         = ''
    url             = 'http://wiki.analog.com/resources/tools-software/linux-software/libiio'
    description     = 'Library for interfacing with IIO devices'
    settings        = 'os', 'compiler', 'build_type', 'arch'
    options         = {'shared': [True, False]}
    default_options = 'shared=False'
    generators      = 'cmake'
    requires        = 'xml2/2.9.8@ntc/stable',

    def source(self):
        g = tools.Git(folder=self.name)
        g.clone('https://github.com/analogdevicesinc/libiio.git', branch='v%s'%self.version)

    def build(self):
        cmake = CMake(self)
        cmake.definitions['BUILD_SHARED_LIBS:BOOL'] = 'ON' if self.options.shared else 'OFF'

        if 'TARGETMACH' in os.environ:
            cmake.definitions['PTHREAD_LIBRARIES'] = os.path.join(os.environ['TOOLCHAIN_ROOT'], 'libc', 'usr', 'lib', 'libpthread.so')

        # libxml2 is found using pkg-config

        cmake.configure(source_folder=self.name)
        cmake.build()
        cmake.install()

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
