#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import os, shutil
from conans import ConanFile, tools, AutoToolsBuildEnvironment


class PinentryConan(ConanFile):
    name            = 'pinentry'
    version         = '1.1.0'
    md5_hash        = '3829315cb0a1e9cedc05ffe6def7a2c6'
    license         = 'Creative Commons Attribution-ShareAlike 3.0 Unported'
    url             = 'https://www.pinentry.org/download/index.en.html'
    description     = 'Pinentry is a collection of passphrase entry dialogs which is required for almost all usages of GnuPG.'
    settings        = 'os', 'compiler', 'build_type', 'arch'
    requires        = (
        'gpg_error/1.30@ntc/stable',
        'assuan/2.5.1@ntc/stable',
        'ncurses/6.1@ntc/stable',
    )
    options = {
        'deploy_prefix': 'ANY',
    }
    default_options = (
        'deploy_prefix=/opt/gnupg',
    )

    def source(self):
        url = f'https://www.gnupg.org/ftp/gcrypt/pinentry/pinentry-{self.version}.tar.bz2'
        archive = os.path.basename(url)
        tools.download(url=url, filename=archive)

        # Ironically, I don't know how to use the .sig file, so we use the md5hash
        tools.check_md5(archive, self.md5_hash)

        tools.unzip(archive)
        shutil.move(f'pinentry-{self.version}', self.name)

    def build(self):

        args = []
        if 'TARGETMACH' in os.environ:
            args.append('--host=%s'%os.environ['TARGETMACH'])

        args.append(f'--prefix={self.package_folder}')
        args.append('--with-libgpg-error-prefix=%s'%self.deps_cpp_info['gpg_error'].rootpath)
        args.append('--with-libassuan-prefix=%s'%self.deps_cpp_info['assuan'].rootpath)
        args.append('--with-ncurses-include-dir=%s'%os.path.join(self.deps_cpp_info['ncurses'].rootpath, self.deps_cpp_info['ncurses'].includedirs[0]))

        # Oour ncurses doesn't provite a pkg-config
        env = {}
        env['NCURSES_CFLAGS'] = ' '.join(['-I%s'%os.path.join(self.deps_cpp_info['ncurses'].rootpath, d) for d in self.deps_cpp_info['ncurses'].includedirs + [os.path.join(self.deps_cpp_info['ncurses'].includedirs[0], 'ncursesw')]])
        env['NCURSES_LIBS'] = \
            ' '.join(['-L%s'%os.path.join(self.deps_cpp_info['ncurses'].rootpath, d) for d in self.deps_cpp_info['ncurses'].libdirs]) \
            + ' ' \
            + ' '.join('-l%s'%l for l in set(self.deps_cpp_info['ncurses'].libs))

        # Debug
        s = []
        for k,v in env.items():
            s.append(f'- {k}={v}')
        self.output.info('Additional Environment:\n%s'%'\n'.join(s))

        autotools = AutoToolsBuildEnvironment(self)
        with tools.chdir(self.name):
            with tools.environment_append(env):
                autotools.configure(args=args)
                autotools.make()

    def package(self):
        autotools = AutoToolsBuildEnvironment(self)
        with tools.chdir(self.name):
            autotools.make(args=['install'])

    def deploy(self):
        # Copy our executables to a common location
        try:
            if not os.path.exists(str(self.options.deploy_prefix)):
                os.makedirs(str(self.options.deploy_prefix))

            self.copy(pattern='*', src=os.path.join(self.package_folder, 'bin'), dst=os.path.join(str(self.options.deploy_prefix), 'bin'))
        except PermissionError:
            self.output.warn("Could not deploy Pinentry's executables to %s.")

    def package_info(self):
        pass

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
