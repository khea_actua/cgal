#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import os, shutil
from conans import ConanFile, tools, AutoToolsBuildEnvironment


class KsbaConan(ConanFile):
    name            = 'ksba'
    version         = '1.3.5'
    md5_hash        = '8302a3e263a7c630aa7dea7d341f07a2'
    license         = 'Creative Commons Attribution-ShareAlike 3.0 Unported'
    url             = 'https://www.gnupg.org/download/index.en.html'
    description     = 'KSBA is the standard library to access GnuPG functions from programming languages.'
    settings        = 'os', 'compiler', 'build_type', 'arch'
    requires        = (
        'gpg_error/1.30@ntc/stable',
    )
    options = {'deploy_prefix': 'ANY'}
    default_options = (
        'deploy_prefix=/opt/gnupg'
    )

    def source(self):
        url = f'https://www.gnupg.org/ftp/gcrypt/libksba/libksba-{self.version}.tar.bz2'
        archive = os.path.basename(url)
        tools.download(url=url, filename=archive)

        # Ironically, I don't know how to use the .sig file, so we use the md5hash
        tools.check_md5(archive, self.md5_hash)

        tools.unzip(archive)
        shutil.move(f'libksba-{self.version}', self.name)

    def build(self):

        args = []
        if 'TARGETMACH' in os.environ:
            args.append('--host=%s'%os.environ['TARGETMACH'])

        args.append(f'--prefix={self.package_folder}')
        args.append('--with-libgpg-error-prefix=%s'%self.deps_cpp_info['gpg_error'].rootpath)

        autotools = AutoToolsBuildEnvironment(self)
        with tools.chdir(self.name):
            autotools.configure(args=args)
            autotools.make()

    def package(self):
        autotools = AutoToolsBuildEnvironment(self)
        with tools.chdir(self.name):
            autotools.make(args=['install'])

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

    def deploy(self):
        try:
            for d in self.cpp_info.libdirs:
                self.copy(pattern='*', src=os.path.join(self.package_folder, d), dst=os.path.join(str(self.options.deploy_prefix), d), symlinks=True)
        except PermissionError:
            self.output.warn("Could not copy libraries to %s."%str(self.options.deploy_prefix))

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
