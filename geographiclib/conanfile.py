#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

from conans import ConanFile, CMake


class GeographiclibConan(ConanFile):
    """ Tested with 1.37, 1.46, and 1.49 """

    name        = 'geographiclib'
    license     = 'https://geographiclib.sourceforge.io/html/LICENSE.txt'
    url         = 'https://geographiclib.sourceforge.io/html/'
    description = 'GeographicLib is a small set of C++ classes for performing conversions between geographic, UTM, UPS, MGRS, geocentric, and local cartesian coordinates, for gravity (e.g., EGM2008), geoid height and geomagnetic field (e.g., WMM2015) calculations, and for solving geodesic problems.'
    settings    = 'os', 'compiler', 'build_type', 'arch'

    options     = {
        'shared':      [True, False],
    }
    default_options = (
        'shared=True',
    )


    def source(self):
        self.run(f'git clone git://git.code.sourceforge.net/p/geographiclib/code {self.name}')
        self.run(f'cd {self.name} && git checkout v{self.version}')

    def build(self):
        cmake = CMake(self)

        cmake.definitions['BUILD_SHARED_LIBS:BOOL'] = 'TRUE' if self.options.shared else 'FALSE'
        if 'Linux' == self.settings.os:
            cmake.definitions['CMAKE_POSITION_INDEPENDENT_CODE'] = 'ON'

        cmake.configure(source_folder=self.name)
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        pass

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
