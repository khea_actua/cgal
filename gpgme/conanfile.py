#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import os, shutil
from conans import ConanFile, tools, AutoToolsBuildEnvironment


class GpgmeConan(ConanFile):
    name            = 'gpgme'
    version         = '1.11.1'
    md5_hash        = '129c46fb85a7ffa41e43345e48aee884'
    license         = 'Creative Commons Attribution-ShareAlike 3.0 Unported'
    url             = 'https://www.gnupg.org/download/index.en.html'
    description     = 'GPGME is the standard library to access GnuPG functions from programming languages.'
    settings        = 'os', 'compiler', 'build_type', 'arch'
    requires        = (
        'gpg_error/1.30@ntc/stable',
        'assuan/2.5.1@ntc/stable',
    )
    options = {
        'tests': [True, False],
        'deploy_prefix': 'ANY'
    }
    default_options = (
        'tests=False',
        'deploy_prefix=/opt/gnupg'
    )

    def source(self):
        url = f'https://www.gnupg.org/ftp/gcrypt/gpgme/gpgme-{self.version}.tar.bz2'
        archive = os.path.basename(url)
        tools.download(url=url, filename=archive)

        # Ironically, I don't know how to use the .sig file, so we use the md5hash
        tools.check_md5(archive, self.md5_hash)

        tools.unzip(archive)
        shutil.move(f'gpgme-{self.version}', self.name)

    def build(self):

        args = []
        if 'TARGETMACH' in os.environ:
            args.append('--host=%s'%os.environ['TARGETMACH'])

        args.append(f'--prefix={self.package_folder}')
        args.append('--with-libgpg-error-prefix=%s'%self.deps_cpp_info['gpg_error'].rootpath)
        args.append('--with-libassuan-prefix=%s'%self.deps_cpp_info['assuan'].rootpath)

        if not self.options.tests:
            args.append('--disable-gpgconf-test')
            args.append('--disable-gpg-test')
            args.append('--disable-gpgsm-test')
            args.append('--disable-g13-test')

        autotools = AutoToolsBuildEnvironment(self)
        with tools.chdir(self.name):
            # with tools.environment_append(env_vars):
            autotools.configure(args=args)
            autotools.make()
            autotools.make(args=['install'])

    def package(self):
        autotools = AutoToolsBuildEnvironment(self)
        with tools.chdir(self.name):
            autotools.make(args=['install'])

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

    def deploy(self):
        try:
            for d in self.cpp_info.libdirs:
                self.copy(pattern='*', src=os.path.join(self.package_folder, d), dst=os.path.join(str(self.options.deploy_prefix), d), symlinks=True)
        except PermissionError:
            self.output.warn("Could not copy libraries to %s."%str(self.options.deploy_prefix))

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
