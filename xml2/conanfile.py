#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import shutil, os, platform
from conans import ConanFile, tools, AutoToolsBuildEnvironment


class Xml2Conan(ConanFile):
    name            = 'xml2'
    license         = ''
    version         = '2.9.8'
    md5_hash        = 'b786e353e2aa1b872d70d5d1ca0c740d'
    url             = 'http://wiki.analog.com/resources/tools-software/linux-software/libiio'
    description     = 'Library for interfacing with IIO devices'
    settings        = 'os', 'compiler', 'build_type', 'arch'
    options         = {'shared': [True, False]}
    default_options = 'shared=False'
    requires        = 'zlib/[>=1.2.11]@conan/stable', 'helpers/[>=0.3.0]@ntc/stable'

    def source(self):
        archive = f'libxml2-{self.version}'
        self.output.info(f'Downloading from ftp://xmlsoft.org/libxml2/{archive}.tar.gz')
        tools.ftp_download(
            ip='xmlsoft.org',
            filename=f'libxml2/{archive}.tar.gz'
        )
        tools.check_md5(f'{archive}.tar.gz', self.md5_hash)
        tools.unzip(f'{archive}.tar.gz')
        shutil.move(archive, self.name)
        os.unlink(f'{archive}.tar.gz')

    def build(self):
        args = []

        if 'TARGETMACH' in os.environ:
            args.append('--host=%s'%os.environ['TARGETMACH'])

        args.append('--without-python')
        args.append(f'--prefix={self.package_folder}')

        env_vars = {}
        env_vars['PKG_CONFIG_ZLIB_PREFIX'] = self.deps_cpp_info['zlib'].rootpath
        pkg_config_path = [self.deps_cpp_info['zlib'].rootpath]
        env_vars['PKG_CONFIG_PATH'] = (';' if 'Windows' == platform.system else ':').join(pkg_config_path)

        autotools = AutoToolsBuildEnvironment(self)
        with tools.chdir(self.name):
            with tools.environment_append(env_vars):
                autotools.configure(args=args)
                autotools.make()
                autotools.make(args=['install'])

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

        with tools.pythonpath(self):
            from platform_helpers import adjustPath, prependPkgConfigPath

            self.env_info.PKG_CONFIG_LIBXML_2_0_PREFIX = adjustPath(self.package_folder)
            prependPkgConfigPath(adjustPath(os.path.join(self.package_folder, 'lib', 'pkgconfig')), self.env_info)

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
