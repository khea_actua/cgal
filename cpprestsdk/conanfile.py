#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import os, shutil
from conans import ConanFile, CMake, tools
from conans.model.version import Version


class CpprestsdkConan(ConanFile):
    """ Tested with 2.8.0 and 2.10.1 """

    name            = 'cpprestsdk'
    license         = 'Apache License 2.0'
    description     = 'The C++ REST SDK is a Microsoft project for cloud-based client-server communication in native code using a modern asynchronous C++ API design. This project aims to help C++ developers connect to and interact with services.'
    url             = 'https://github.com/Microsoft/cpprestsdk'
    settings        = 'os', 'compiler', 'build_type', 'arch', 'arch_build'
    options         = {
        'shared':             [True, False],
        'exclude_websockets': [True, False],
        'boost_debug':        [True, False],
        'cxx11':              [True, False],
    }
    default_options = (
        'shared=True',
        'exclude_websockets=False',
        'boost_debug=False',
        'cxx11=True',
    )
    build_policy    = 'missing'
    generators      = 'cmake'
    requires = (
        'boost/[>1.46]@ntc/stable',
        # 'OpenSSL/1.0.2n@conan/stable',
        'OpenSSL/1.1.0i@conan/stable',
    )

    # Custom CMakeLists file we use to help setup the CMake env.
    exports = 'CMakeLists.txt'

    def source(self):
        ext = 'tar.gz'
        archive = f'{self.version}.{ext}'
        url = f'https://github.com/Microsoft/cpprestsdk/archive/v{archive}'
        tools.download(url=url, filename=archive)

        hashes = {
            '2.10.1.tar.gz': 'bed28c3685beee9c977ae1bf5ae7e9ef',
            '2.8.0.tar.gz':  '60c2cb49b603a6b69516ca5061ab263d',
            '2.7.0.tar.gz':  '02b2602f3b65fe806fdcdc5ef76604c3',
        }

        tools.check_md5(archive, hashes[archive])

        tools.unzip(archive)
        shutil.move(src=f'cpprestsdk-{self.version}', dst=self.name)
        os.unlink(archive)

        # Place our own CMakeLists file at the top.  In here, we can inject
        # conan targets, and more easily modify the CMake variables.
        shutil.move(
            os.path.join(self.name, 'Release', 'CMakeLists.txt'),
            os.path.join(self.name, 'Release', 'orig.CMakeLists.txt')
        )
        shutil.move(src='CMakeLists.txt', dst=os.path.join(self.name, 'Release', 'CMakeLists.txt'))

        # This is to avoid the alignment error
        #    /include/boost/core/addressof.hpp:41:75: error: cast from 'char*' to 'const std::basic_string<char>*' increases required alignment of target type [-Werror=cast-align]
        # when cross-compiling.  Excluding websockets was proposed
        # https://github.com/Microsoft/cpprestsdk/issues/59 , but then
        # boost (which is needed by everything else) simply isn't included.
        tools.replace_in_file(
            file_path = os.path.join(self.name, 'Release', 'src', 'CMakeLists.txt'),
            search = '-Werror',
            replace = ''
        )
        # Maybe this can be removed in a higher version?

        # CppRestSDK is hard set to use static boost libs, but there's no
        # explanation as to why.. So we're removing that hard constraint here.
        tools.replace_in_file(
            file_path = os.path.join(self.name, 'Release', 'libs', 'websocketpp', 'CMakeLists.txt'),
            search = 'set (Boost_USE_STATIC_LIBS TRUE)',
            replace = 'set (Boost_USE_STATIC_LIBS %s) # Modified out in conan source()'%('FALSE' if self.options['boost'].shared else 'TRUE')
        )
        tools.replace_in_file(
            file_path = os.path.join(self.name, 'Release', 'src/CMakeLists.txt'),
            search = '# Portions specific to cpprest binary versioning.',
            replace = '# Disable auto-linking on MSVC, otherwise MSVC will look for libboost...lib, regardless of what is specified or even in it\'s Additional Dependencies.\ntarget_compile_definitions(${Casablanca_LIBRARY} PRIVATE $<$<BOOL:${MSVC}>:BOOST_ALL_NO_LIB>)\n\n# Portions specific to cpprest binary versioning.',
            strict=False,
        )

        tools.replace_in_file(
            file_path = os.path.join(self.name, 'Release', 'orig.CMakeLists.txt'),
            search = 'option(Boost_USE_STATIC_LIBS ON)',
            replace = 'option(Boost_USE_STATIC_LIBS %s) # Modified out in conan source()'%('OFF' if self.options['boost'].shared else 'ON'),
            strict=False
        )

    def build(self):

        def libName(lib):
            prefix = ''
            if tools.os_info.is_linux:
                prefix = 'lib'
                ext = 'so' if self.options['OpenSSL'].shared else 'a'
            else:
                ext = 'lib'

            return f'{prefix}{lib}.{ext}'


        cmake = CMake(self)

        if self.options.cxx11:
            cmake.definitions['CMAKE_CXX_STANDARD'] = 11

        cmake.definitions['BUILD_TESTS:BOOL']           = 'OFF'
        cmake.definitions['BUILD_SAMPLES:BOOL']         = 'OFF'
        cmake.definitions['BUILD_SHARED_LIBS:BOOL']     = 'ON' if self.options.shared else 'OFF'
        cmake.definitions['BOOST_ROOT:PATH']            = self.deps_cpp_info['boost'].rootpath
        cmake.definitions['Boost_USE_STATIC_LIBS:BOOL'] = 'FALSE' if self.options['boost'].shared else 'TRUE'
        cmake.definitions['Boost_DEBUG:BOOL']           = 'TRUE' if self.options.boost_debug else 'FALSE'

        cmake.definitions['CPPREST_EXCLUDE_WEBSOCKETS:BOOL'] = 'TRUE' if self.options.exclude_websockets else 'FALSE'

        if 'OpenSSL' in self.deps_cpp_info.deps:
            cmake.definitions['OPENSSL_SSL_LIBRARY:PATH']    = os.path.join(self.deps_cpp_info['OpenSSL'].rootpath, self.deps_cpp_info['OpenSSL'].libdirs[0], libName('ssl'))
            cmake.definitions['OPENSSL_INCLUDE_DIR:PATH']    = os.path.join(self.deps_cpp_info['OpenSSL'].rootpath, self.deps_cpp_info['OpenSSL'].includedirs[0])
            cmake.definitions['OPENSSL_CRYPTO_LIBRARY:PATH'] = os.path.join(self.deps_cpp_info['OpenSSL'].rootpath, self.deps_cpp_info['OpenSSL'].libdirs[0], libName('crypto'))

        # Note, the WERROR option was introduced in 3.0.0, so until you're
        # using that version, CMake will throw a warning about it.
        v = Version(str(self.version))
        if v >= '2.10':
            cmake.definitions['WERROR'] = 'Off'

        # Debug
        s = ''
        for k,v in cmake.definitions.items(): s += f' - {k}={v}\n'
        self.output.info('CMake definitions:\n' + s)

        cmake.configure(source_folder=os.path.join(self.name, 'Release'))
        cmake.build()
        # We'll do our own 'install'

    def package(self):
        self.copy('*', dst=os.path.join('include', 'cpprest'), src=os.path.join('cpprestsdk', 'Release', 'include', 'cpprest'), keep_path=False)
        self.copy('*', dst=os.path.join('include', 'cpprest', 'details'), src=os.path.join('cpprestsdk', 'Release', 'include', 'cpprest', 'details'), keep_path=False)
        self.copy('*', dst=os.path.join('include', 'pplx'), src=os.path.join('cpprestsdk', 'Release', 'include', 'pplx'), keep_path=False)

        self.copy('*.so',    dst='lib',  keep_path=False)
        self.copy('*.so.*',  dst='lib',  keep_path=False)
        self.copy('*.a',     dst='lib',  keep_path=False)

        self.copy('*.dll',   dst='bin',  keep_path=False)
        self.copy('*.lib',   dst='lib',  keep_path=False)

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
