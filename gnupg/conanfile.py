#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import os, shutil
from conans import ConanFile, tools, AutoToolsBuildEnvironment


class GnupgConan(ConanFile):
    name            = 'gnupg'
    version         = '2.2.7'
    md5_hash        = 'fc13424af7747a5dd6edb6086ec0cb2f'
    license         = 'Creative Commons Attribution-ShareAlike 3.0 Unported'
    url             = 'https://www.gnupg.org/download/index.en.html'
    description     = 'GPGME is the standard library to access GnuPG functions from programming languages.'
    settings        = 'os', 'compiler', 'build_type', 'arch'
    exports         = 'patch'
    requires        = (
        'gpg_error/1.30@ntc/stable',
        'assuan/2.5.1@ntc/stable',
        'gcrypt/1.8.2@ntc/stable',
        'ksba/1.3.5@ntc/stable',
        'ntbtls/0.1.2@ntc/stable',
        'npth/1.5@ntc/stable',
        'zlib/1.2.11@conan/stable',
        'bzip2/1.0.6@lasote/stable',
    )
    options = {'deploy_prefix': 'ANY'}
    default_options = (
        'deploy_prefix=/opt/gnupg'
    )

    def source(self):
        url = f'https://www.gnupg.org/ftp/gcrypt/gnupg/gnupg-{self.version}.tar.bz2'
        archive = os.path.basename(url)
        tools.download(url=url, filename=archive)

        # Ironically, I don't know how to use the .sig file, so we use the md5hash
        tools.check_md5(archive, self.md5_hash)

        tools.unzip(archive)
        shutil.move(f'gnupg-{self.version}', self.name)

        # Fix some syntax errors
        tools.patch(
            base_path=os.path.join(self.name),
            patch_file='patch'
        )

    def build(self):

        args = []
        if 'TARGETMACH' in os.environ:
            args.append('--host=%s'%os.environ['TARGETMACH'])

        args.append(f'--prefix={self.package_folder}')
        args.append('--with-libgpg-error-prefix=%s'%self.deps_cpp_info['gpg_error'].rootpath)
        args.append('--with-libassuan-prefix=%s'%self.deps_cpp_info['assuan'].rootpath)
        args.append('--with-libgcrypt-prefix=%s'%self.deps_cpp_info['gcrypt'].rootpath)
        args.append('--with-ksba-prefix=%s'%self.deps_cpp_info['ksba'].rootpath)
        args.append('--with-npth-prefix=%s'%self.deps_cpp_info['npth'].rootpath)
        args.append('--with-zlib-prefix=%s'%self.deps_cpp_info['zlib'].rootpath)
        args.append('--with-bzip2-prefix=%s'%self.deps_cpp_info['bzip2'].rootpath)

        # gnupg hardcodes the bin path in to the executables so that it can
        # call them.  This obviously works terribly with a package manager, so
        # the bin path must be a standard path known at configuration time.
        try:
            if not os.path.exists(str(self.options.deploy_prefix)):
                os.makedirs(str(self.options.deploy_prefix))
            args.append('--bindir=%s'%os.path.join(str(self.options.deploy_prefix), 'bin'))
            args.append('--sbindir=%s'%os.path.join(str(self.options.deploy_prefix), 'sbin'))
        except PermissionError:
            self.output.error("Could not write GNUPG's executables to %s.  Due to a GNUPG quirk, the executables locate each other by absolute path set at configure time.  By not being able to write to a bin path, the resulting executables will be unusable.")

        autotools = AutoToolsBuildEnvironment(self)
        with tools.chdir(self.name):
            autotools.configure(args=args)
            autotools.make()

    def package(self):
        autotools = AutoToolsBuildEnvironment(self)
        with tools.chdir(self.name):
            autotools.make(args=['install'])

        # Make sure our executables are in our package
        for p in ['gpg*', 'kbxutil', 'dirmngr*', 'watchgnupg']:
            self.copy(pattern=p, src=os.path.join(str(self.options.deploy_prefix), 'bin'), dst=os.path.join(self.package_folder, 'bin'))
        for p in ['applygnupgdefaults', 'addgnupghome']:
            self.copy(pattern='*', src=os.path.join(str(self.options.deploy_prefix), 'sbin'), dst=os.path.join(self.package_folder, 'sbin'))

    def deploy(self):
        # Copy our executables to a common location
        try:
            if not os.path.exists(str(self.options.deploy_prefix)):
                os.makedirs(str(self.options.deploy_prefix))

            self.copy(pattern='*', src=os.path.join(self.package_folder, 'bin'), dst=os.path.join(str(self.options.deploy_prefix), 'bin'))
            self.copy(pattern='*', src=os.path.join(self.package_folder, 'sbin'), dst=os.path.join(str(self.options.deploy_prefix), 'sbin'))
        except PermissionError:
            self.output.warn("Could not copy GNUPG's executables to %s.  Due to a GNUPG quirk, the executables locate each other by absolute path set at configure time.  Therefore, since you cannot deploy the executables to a known location, some of them will not function properly."%str(self.options.deploy_prefix))

    def package_info(self):
        pass

        # libs from the build directory don't seem to be installed.  This might
        # be fine as they're all static and probably built into the
        # executables.  For reference, they're libkeybox*, libsimple-pwquery,
        # libgpggrl, libcommon*
        # self.cpp_info.libdirs = ['common', 'kbx']
        # self.cpp_info.libs = [tools.collect_libs(self, d) for d in self.cpp_info.libdirs]

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
