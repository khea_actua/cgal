#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import os
from conans import ConanFile, CMake, tools

class QhullConan(ConanFile):
    """
    Build QHull.  Note, QHULL's CMake explicitly marks the libraries as STATIC,
    therefore there is (and can be no) 'shared' option.
    """

    name           = 'qhull'
    version        = '2015.2'
    version_commit = '5a79a00'
    src_dir        = f'{name}-{version}'
    license        = '<Put the package license here>'
    url            = 'http://www.qhull.org/'
    settings       = 'os', 'compiler', 'build_type', 'arch'
    requires       = 'helpers/[>=0.3]@ntc/stable'
    options         = {
        'fPIC':   [True, False],
        'cxx11':  [True, False],
    }
    default_options = 'fPIC=True', 'cxx11=True'
    generators = 'cmake'

    def config_options(self):
        if self.settings.compiler == "Visual Studio":
            self.options.remove("fPIC")

    def source(self):
        self.output.info(f'Cloning: git clone https://github.com/qhull/qhull.git {self.src_dir}')
        self.run(f'git clone https://github.com/qhull/qhull.git {self.src_dir}')
        self.run(f'cd {self.src_dir} && git checkout {self.version_commit}')

        if self.settings.compiler == 'gcc':
            import cmake_helpers
            cmake_helpers.wrapCMakeFile(os.path.join(self.source_folder, self.src_dir), output_func=self.output.info)

    def build(self):
        cmake = CMake(self)

        if 'fPIC' in self.options and self.options.fPIC:
            cmake.definitions['CMAKE_POSITION_INDEPENDENT_CODE'] = 'ON'

        if self.options.cxx11:
            cmake.definitions['CMAKE_CXX_STANDARD'] = 11

        if self.settings.compiler == 'gcc':
            cmake.definitions['ADDITIONAL_CXX_FLAGS:STRING'] = '-frecord-gcc-switches'

        cmake.configure(source_folder=self.src_dir)
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.configure(source_folder=self.src_dir)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

        if 'Windows' == self.settings.os:
            # Add our bin/ to PATH
            self.env_info.path.append(os.path.join(self.package_folder, 'bin'))

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
