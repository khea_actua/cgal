#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import os
from conans import ConanFile, CMake, tools
from conans.errors import ConanException
from conans.model.version import Version


class WtConan(ConanFile):
    """ Tested with 3.3.4, 3.3.10, 3.3.11 """

    name         = 'wt'
    license      = 'GNU General Public License (GPL)'
    url          = 'https://www.webtoolkit.eu/wt'
    description  = 'Web Toolkit'
    settings     = 'os', 'compiler', 'build_type', 'arch'
    build_policy = 'missing'
    generators   = 'cmake'
    requires = (
        'OpenSSL/1.1.1a@conan/stable',
        'sqlite3/[>=3.21.0]@bincrafters/stable',
        'helpers/[>=0.3]@ntc/stable',
        # TODO add Qt
    )
    options = {
        'shared':   [True, False],
        'mysql':    [True, False],
        'pgsql':    [True, False],
        'sqlite':   [True, False],
        'ssl':      [True, False],
        'examples': [True, False],
        'tests':    [True, False],
    }
    default_options = ('shared=True', 'mysql=False', 'pgsql=False', 'sqlite=True', 'ssl=True', 'examples=False', 'tests=False')

    def requirements(self):
        if Version(str(self.version)) <= '3.3.4':
            self.requires('boost/[>=1.56.0,<1.66.0]@ntc/stable')
        elif Version(str(self.version)) <= '3.3.11':
            self.requires('boost/[>=1.56.0,<1.68.0]@ntc/stable')
        else:
            self.requires('boost/[>=1.56.0]@ntc/stable')

    def source(self):
        from source_cache import copyFromCache
        if copyFromCache(f'wt-{self.version}.tar.gz'):
            tools.unzip(f'wt-{self.version}.tar.gz')
            os.rename(f'wt-{self.version}', 'wt')
        else:
            md5s = {
                '4.0.4':  'ebb8f61b111578984f08f52c58104a2b',
                '3.3.10': '1aace13e7a98d739c8ca57e26ab0d52a',
                '3.3.10': 'e9fac6af4bc1245b63f36a691273526b',
                '3.3.9':  'e9fac6af4bc1245b63f36a691273526b',
                '3.3.8':  'f0527e8dcfd82e2487f526131c23ead3'
            }
            if self.version in md5s:
                self.output.info('Downloading Wt archive')
                tools.get(f'https://github.com/emweb/wt/archive/{self.version}.tar.gz', md5=md5s[self.version])
                os.rename(f'wt-{self.version}', 'wt')
            else:
                self.output.info('Cloning Wt repository')
                self.run(f'git clone https://github.com/emweb/wt {self.name}')
                self.run(f'cd {self.name} && git checkout {self.version}')

        if self.version == '3.3.4':
            tools.replace_in_file(
                file_path=f'{self.name}/src/Wt/Render/CssParser.C',
                search='#define WIN32_LEAN_AND_MEAN',
                replace='#define WIN32_LEAN_AND_MEAN\n\n#define BOOST_PHOENIX_NO_VARIADIC_EXPRESSION'
            )

    def build(self):
        def libName(lib):
            prefix = ''
            if tools.os_info.is_linux:
                prefix = 'lib'
                ext = 'so' if self.options['OpenSSL'].shared else 'a'
            else:
                ext = 'lib'

            return f'{prefix}{lib}.{ext}'

        # wt_release = int(self.version.split('.')[0])
        # boost_major = int(self.deps_cpp_info['boost'].version.split('.')[1])
        # if wt_release < 4 and boost_major >= 66:
        wt_version    = Version(str(self.version))
        boost_version = Version(str(self.deps_cpp_info['boost'].version))
        if boost_version > '1.65' and wt_version < '3.3.8':
            raise ConanException('Wt<3.3.8 cannot be build with boost>=1.66')

        cmake = CMake(self)
        cmake.definitions['BOOST_DYNAMIC:BOOL']     = 'TRUE' if self.options['boost'].shared else 'FALSE'
        cmake.definitions['SHARED_LIBS:BOOL']       = 'TRUE' if self.options.shared else 'FALSE'
        cmake.definitions['BUILD_SHARED_LIBS:BOOL'] = 'TRUE' if self.options.shared else 'FALSE'
        cmake.definitions['BOOST_PREFIX:PATH']      = self.deps_cpp_info['boost'].rootpath
        cmake.definitions['BOOST_ROOT:PATH']        = self.deps_cpp_info['boost'].rootpath
        cmake.definitions['WT_NO_SLOT_MACROS']      = 1
        cmake.definitions['WT_CPP_11_MODE:STRING']  = '-std=c++11'
        cmake.definitions['ENABLE_MYSQL:BOOL']      = 'TRUE' if self.options.mysql else 'FALSE'
        cmake.definitions['ENABLE_POSTGRES:BOOL']   = 'TRUE' if self.options.pgsql else 'FALSE'

        cmake.definitions['ENABLE_SSL:BOOL'] = 'True' if self.options.ssl else 'False'
        if self.options.ssl:
            cmake.definitions['SSL_PREFIX:PATH'] = self.deps_cpp_info['OpenSSL'].rootpath
            cmake.definitions['SSL_LIB:PATH'] = os.path.join(self.deps_cpp_info['OpenSSL'].rootpath, self.deps_cpp_info['OpenSSL'].libdirs[0], libName('ssl'))

        cmake.definitions['ENABLE_SQLITE:BOOL']  = 'TRUE' if self.options.sqlite else 'FALSE'
        cmake.definitions['SQLITE3_PREFIX:PATH'] = self.deps_cpp_info['sqlite3'].rootpath

        cmake.definitions['BUILD_TESTS:BOOL']    = 'TRUE' if self.options.tests    else 'FALSE'
        cmake.definitions['BUILD_EXAMPLES:BOOL'] = 'TRUE' if self.options.examples else 'FALSE'

        if self.version in ['3.3.4']:
            cmake.definitions['CONFDIR:PATH']              = f'{self.package_folder}/etc/wt'
            cmake.definitions['CONFIGDIR:PATH']            = f'{self.package_folder}/etc/wt'
            cmake.definitions['CONFIGURATION:PATH']        = f'{self.package_folder}/etc/wt/wt_config.xml'
            cmake.definitions['WTHTTP_CONFIGURATION:PATH'] = f'{self.package_folder}/etc/wt/wthttpd'

        # Debug
        s = '\nCMake Definitions:\n'
        for k,v in cmake.definitions.items():
            s += ' - %s=%s\n'%(k, v)
        self.output.info(s)

        cmake.configure(source_folder=self.name)
        cmake.build()
        cmake.install()

    def package(self):
        pass

    def package_info(self):
        self.cpp_info.resdirs = [os.path.join('share', 'Wt', 'resources')]

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
