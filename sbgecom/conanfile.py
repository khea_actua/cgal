#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, shutil
from conans import ConanFile, CMake, tools


class SbgeclientConan(ConanFile):
    name            = 'sbgecom'
    version         = '1.0.66'
    license         = 'NTC'
    author          = 'Matthew Russell'
    url             = 'http://ntctrac:9090/matt/conan-build-files'
    description     = 'sbGE Client'
    requires        = ('helpers/[>=0.3]@ntc/stable')

    settings        = {
        'os':         ['Linux', 'Windows'],
        'compiler':   None,
        'build_type': None,
        'arch':       ['x86_64', 'x86'],
    }

    def source(self):
        archive_file = 'sbgECom.1.0.66.tar.gz'
        from source_cache import copyFromCache

        if copyFromCache(archive_file):
            tools.unzip(archive_file)
            shutil.move('sbgECom', self.name)
        else:
            self.output.error('sgbECom source is not publically available.')

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder=os.path.join(self.name, 'projects', 'unix'))
        cmake.build()

    def package(self):
        self.copy('*.a' if tools.os_info.is_linux else '*.lib', dst='lib', keep_path=False)
        self.copy('*.h', src=os.path.join(self.source_folder, self.name, 'src'), dst='include')
        self.copy('*.h', src=os.path.join(self.source_folder, self.name, 'common'), dst='include')

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
