#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile


class JchartConan(ConanFile):
    name        = 'jchart'
    version     = '3.2.2'
    author      = 'Matthew Russell'
    url         = 'git@ntctrac.corp.neptec.com:matt/conan-build-files.git'
    description = 'JChart used for LCM'
    exports      = 'jchart/*'

    def package(self):
        self.copy('jchart/*', keep_path=False)

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
