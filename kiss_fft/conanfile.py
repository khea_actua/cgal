#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import sys, re, os
from conans import ConanFile, tools, AutoToolsBuildEnvironment
from conans.errors import ConanException


class KissfftConan(ConanFile):
    """
    Build kiss_fft.  On Windows, the source doesn't export any symbols, and
    this lib is only used in one place, so we only build a statuc lib.  There
    is commented code throughout here to guide in potentically making a shared
    lib if there's ever interest.
    """

    name        = 'kiss_fft'
    version     = '1.3.0'
    commit      = 'e797a0e'
    license     = 'Revised BSD License'
    url         = 'http://kissfft.sourceforge.net'
    description = 'A mixed-radix Fast Fourier Transform based up on the principle, "Keep It Simple, Stupid."'
    settings    = 'os', 'compiler', 'build_type', 'arch'

    options     = {
        'msvc':       [12, 15],
    }
    default_options = (
        'msvc=12'
    )

    def source(self):
        self.run(f'git clone https://github.com/itdaniher/kissfft.git {self.name}')
        self.run(f'cd {self.name} && git checkout {self.commit}')

    def build(self):

        # Run make in the base directory
        with tools.chdir(self.name):
            autotools = AutoToolsBuildEnvironment(self, win_bash=('Windows' == self.settings.os))

            if 'Windows' == self.settings.os:
                if 'Visual Studio' == self.settings.compiler:
                    self.output.error('Please build with a WSL profile')
                    sys.exit(-1)

                fft_tools = ['kiss_fftndr.o', 'kiss_fftnd.o', 'kiss_fftr.o']
                with tools.chdir('tools'):

                    # Insert a suffix rule to create the object files
                    with open('Makefile') as f: contents = f.read()

                    if not re.search(r'\.c\.o:', contents):
                        contents += '\n\n\n.c.o:\n\t$(CC) -c $< -I.. $(TYPEFLAGS) -DREAL_FASTFIR -DFAST_FILT_UTIL -lm\n'

                    with open('Makefile', 'w') as f: f.write(contents)

                    try:
                        self.output.info('Building required fft tools')
                        autotools.make(args=fft_tools)
                    except ConanException:
                        self.output.error('kiss_fft additional object files compilation failed')
                        sys.exit(-1)

                # All code is fPIC on this target
                tools.replace_in_file(
                    file_path='Makefile',
                    search='-fPIC ',
                    replace='',
                )

                # Add the tools to the main lib
                tools.replace_in_file(
                    file_path='Makefile',
                    search='ar crus libkissfft.a kiss_fft.o',
                    replace='ar crus libkissfft.a kiss_fft.o %s'%(' '.join(f'tools/{f}' for f in fft_tools)),
                )

                # Insert a suffix rule to create the object files
                tools.replace_in_file(
                    file_path='Makefile',
                    search='gcc -shared -Wl,-soname,libkissfft.so -o libkissfft.so kiss_fft.o',
                    replace='gcc -shared -o kiss_fft{debug}.dll kiss_fft.o {tools} -Wl,--out-implib,kiss_fft{debug}.lib'
                        .format(
                            debug=('d' if 'Debug' == self.settings.build_type else ''),
                            tools=' '.join(f'tools/{f}' for f in fft_tools),
                        )
                )


                self.output.info('Building base')
                autotools.make()

                # self._export_dll_symbols()

            elif 'Linux' == self.settings.os:
                # My linux build scripts seemed to require the manipulation below,
                # but I cannot remember why.  So, for now, only going to do this on
                # linux until I run into a windows error or something.

                with tools.chdir('tools'):

                    # Insert a suffix rule to create the object files
                    with open('Makefile') as f: contents = f.read()

                    if not re.search(r'\.c\.o:', contents):
                        contents += '\n\n\n.c.o:\n\t$(CC) -c $< -I.. $(TYPEFLAGS) -DREAL_FASTFIR -DFAST_FILT_UTIL -lm\n'

                    with open('Makefile', 'w') as f: f.write(contents)

                    try:
                        self.output.info('Building fftndr and fftnd')
                        autotools.make(args=['kiss_fftndr.o', 'kiss_fftnd.o'])
                    except ConanException:
                        self.output.error('kiss_fft additional object files compilation failed')
                        sys.exit(-1)

                # Add the objects to the archive
                try:
                    self.run('ar r libkissfft.a tools/kiss_fftndr.o tools/kiss_fftnd.o')
                except ConanException:
                    self.output.error('Could not add kiss_fft tools to kiss_fft tools primary library')

    def _export_dll_symbols(self):
        """
        The source code above does not explicitly export any symbols.  This
        function modifies the DLL in an attempt to export them.

        Note, our working directory should be build/kiss_fft
        """

        self.run('dlltool -z kiss_fft.def --export-all-symbol kiss_fft.dll')

        # Seems like this isn't needed, but the next steps would be to filter
        # the kiss_fft.def file for only the symbols we want, and then to export
        # a new lib.  See instructions at
        # http://www.mingw.org/wiki/createimportlibraries

    def package(self):
        self.copy(pattern='*.h', dst=os.path.join('include', 'kiss_fft'), src=self.build_folder, keep_path=False)
        self.copy(pattern='*.a',  dst='lib', keep_path=False)
        if 'Linux' == self.settings.os:
            self.copy(pattern='*.so', dst='lib', keep_path=False)

            # The o2linux64 version has this lib with an underscore, but the Makefile
            # builds it without... The documentation swtiches back and forth,
            # so simply link one to the other
            with tools.chdir(os.path.join(self.package_folder, 'lib')):
                os.symlink(src='libkissfft.a', dst='libkiss_fft.a')
        else:
            self.copy(pattern='*.lib', dst='lib', keep_path=False)
            self.copy(pattern='*.dll', dst='bin', keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ['kiss_fft']

    def package_id(self):
        # On windows, we cross compile this with mingw.. But because it's
        # compatible with MSVC, set it's hash to reflect that.
        if 'gcc' == self.settings.compiler and 'Windows' == self.settings.os:
            self.info.settings.compiler = 'Visual Studio'
            self.info.settings.compiler.version = int(str(self.options.msvc))

            # Assumed shared
            runtime = 'MD'
            if self.settings.build_type == 'Debug':
                runtime += 'd'
            self.info.settings.compiler.runtime = runtime

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
