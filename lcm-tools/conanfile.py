#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import os
from conans import ConanFile


class LcmtoolsConan(ConanFile):
    name = 'lcm_tools'
    version = '1.1.2'
    license = 'BSD'
    url = 'https://github.com/lcm-proj/lcm'
    description = 'Binary package of the LCM tools.  This was originally created for cross-compiling, where the binaries are required to match the base system to generate the hpp files to built on the host system'
    build_requires = f'lcm/{version}@ntc/stable'

    settings = 'os_build', 'arch_build'

    def package(self):
        for d in self.deps_cpp_info['lcm'].bindirs:
            self.copy('*', dst='bin', src=os.path.join(self.deps_cpp_info['lcm'].rootpath, d))
        for d in self.deps_cpp_info['lcm'].libdirs:
            self.copy('*', dst='lib', src=os.path.join(self.deps_cpp_info['lcm'].rootpath, d))

    def package_info(self):
        self.env_info.path.append(os.path.join(self.package_folder, 'bin'))
        self.env_info.manpath.append(os.path.join(self.package_folder, 'share', 'man'))

        # Libs required for execution.  For linking, add to LD_RUN_PATH
        self.env_info.ld_library_path.append(os.path.join(self.package_folder, 'lib'))

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
