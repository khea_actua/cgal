#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import os, shutil
from conans import ConanFile, tools, AutoToolsBuildEnvironment


class GpgErrorConan(ConanFile):
    name            = 'gpg_error'
    version         = '1.30'
    md5_hash        = 'd5422ab2dbbd5104ce7f4b21db5791b1'
    license         = 'Creative Commons Attribution-ShareAlike 3.0 Unported'
    url             = 'https://www.gnupg.org/download/index.en.html'
    description     = 'libgpg-error is a require depencency of GPGME',
    settings        = 'os', 'compiler', 'build_type', 'arch'
    options = {'deploy_prefix': 'ANY'}
    default_options = (
        'deploy_prefix=/opt/gnupg'
    )

    def source(self):
        url = f'https://www.gnupg.org/ftp/gcrypt/libgpg-error/libgpg-error-{self.version}.tar.bz2'
        archive = os.path.basename(url)
        tools.download(url=url, filename=archive)

        # Ironically, I don't know how to use the .sig file, so we use the md5hash
        tools.check_md5(archive, self.md5_hash)

        tools.unzip(archive)
        shutil.move(f'libgpg-error-{self.version}', self.name)

    def build(self):

        args = []
        if 'TARGETMACH' in os.environ:
            args.append('--host=%s'%os.environ['TARGETMACH'])

        args.append(f'--prefix={self.package_folder}')

        autotools = AutoToolsBuildEnvironment(self)
        with tools.chdir(self.name):
            autotools.configure(args=args)
            autotools.make()
            autotools.make(args=['install'])

    def package(self):
        autotools = AutoToolsBuildEnvironment(self)
        with tools.chdir(self.name):
            autotools.make(args=['install'])

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

    def deploy(self):
        try:
            for d in self.cpp_info.libdirs:
                self.copy(pattern='*', src=os.path.join(self.package_folder, d), dst=os.path.join(str(self.options.deploy_prefix), d), symlinks=True)
        except PermissionError:
            self.output.warn("Could not copy libraries to %s."%str(self.options.deploy_prefix))

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
