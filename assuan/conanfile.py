#!/usr/bin/env python
# -*- coding: future_fstrings -*-
# -*- coding: utf-8 -*-

import os, shutil
from conans import ConanFile, tools, AutoToolsBuildEnvironment


class AssuanConan(ConanFile):
    name            = 'assuan'
    version         = '2.5.1'
    md5_hash        = '4354b7ae296894f232ada226a062d7d7'
    license         = 'Creative Commons Attribution-ShareAlike 3.0 Unported'
    url             = 'ftp://ftp.gnupg.org/gcrypt/libassuan/'
    description     = 'Libassuan is a small library implementing the so-called Assuan protocol. This protocol is used for IPC between most newer GnuPG components. Both, server and client side functions are provided.'
    settings        = 'os', 'compiler', 'build_type', 'arch'
    requires        = (
        'gpg_error/1.30@ntc/stable',
    )
    options = {'deploy_prefix': 'ANY'}
    default_options = (
        'deploy_prefix=/opt/gnupg'
    )

    def source(self):
        archive = f'libassuan-{self.version}'
        archive_ext = '.tar.bz2'
        self.output.info(f'Downloading file ftp://ftp.gnupg.org/gcrypt/libassuan/{archive}{archive_ext}')
        tools.ftp_download(
            ip='ftp.gnupg.org',
            filename=f'gcrypt/libassuan/{archive}{archive_ext}',
        )

        # Ironically, I don't know how to use the .sig file, so we use the md5hash
        tools.check_md5('%s%s'%(archive, archive_ext), self.md5_hash)

        tools.unzip('%s%s'%(archive, archive_ext))
        shutil.move(archive, self.name)

    def build(self):

        args = []
        if 'TARGETMACH' in os.environ:
            args.append('--host=%s'%os.environ['TARGETMACH'])

        args.append(f'--prefix={self.package_folder}')
        args.append('--with-libgpg-error-prefix=%s'%self.deps_cpp_info['gpg_error'].rootpath)

        autotools = AutoToolsBuildEnvironment(self)
        with tools.chdir(self.name):
            autotools.configure(args=args)
            autotools.make()
            autotools.make(args=['install'])

    def package(self):
        autotools = AutoToolsBuildEnvironment(self)
        with tools.chdir(self.name):
            autotools.make(args=['install'])

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

    def deploy(self):
        try:
            for d in self.cpp_info.libdirs:
                self.copy(pattern='*', src=os.path.join(self.package_folder, d), dst=os.path.join(str(self.options.deploy_prefix), d), symlinks=True)
        except PermissionError:
            self.output.warn("Could not copy libraries to %s."%str(self.options.deploy_prefix))

# vim: ts=4 sw=4 expandtab ffs=unix ft=python foldmethod=marker :
